#pragma once

#include "UIElement.h"
#include "Font.h"
#include <string>

class UIText : public UIElement
{
public:
	UIText(std::string fontPath, uint16_t fontSize, std::string text, Vector2i offset, ANCHOR_POINT anchorPoint = ANCHOR_POINT::CENTER, ANCHOR_POINT pivotPoint = ANCHOR_POINT::CENTER);
	~UIText();

	void SetText(std::string text);

private:
	Font* font;
	std::string text;
};