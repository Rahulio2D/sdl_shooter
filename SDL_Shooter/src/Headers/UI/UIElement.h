#pragma once

#include "Vector2.h"
struct SDL_Rect;
class SDL_Texture;

enum class ANCHOR_POINT
{
	TOP_LEFT, TOP_CENTER, TOP_RIGHT, LEFT, CENTER, RIGHT, BOTTOM_LEFT, BOTTOM_CENTER, BOTTOM_RIGHT
};

class UIElement
{
public:
	UIElement(Vector2i offset, ANCHOR_POINT anchorPoint = ANCHOR_POINT::CENTER, ANCHOR_POINT pivotPoint = ANCHOR_POINT::CENTER);
	~UIElement();

	void Render();
	void SetOffset(Vector2i offset);

protected:
	void SetUIPosition();
	void ResetDestRect();
	void ResetSourceRect();

	Vector2i position;
	Vector2i offset;
	Vector2i size;

	SDL_Rect* destRect;
	SDL_Rect* sourceRect;
	SDL_Texture* texture;

	ANCHOR_POINT anchor;	// Anchor is the anchor point in the window
	ANCHOR_POINT pivot;		// Pivot is on the text box itself

	Vector2i anchorPoint;
	Vector2i pivotPoint;
};