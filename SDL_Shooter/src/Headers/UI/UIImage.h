#pragma once

#include "UIElement.h"

class UIImage : public UIElement
{
public:
	UIImage(std::string texturePath, Vector2i size, Vector2i offset, ANCHOR_POINT anchorPoint = ANCHOR_POINT::CENTER, ANCHOR_POINT pivotPoint = ANCHOR_POINT::CENTER);
	UIImage(std::string texturePath, Vector2i size, SDL_Rect sourceRect, Vector2i offset, ANCHOR_POINT anchorPoint = ANCHOR_POINT::CENTER, ANCHOR_POINT pivotPoint = ANCHOR_POINT::CENTER);
	~UIImage();

	void SetSourceRect(SDL_Rect sourceRect);
	void SetImage(std::string texturePath, SDL_Rect sourceRect);
};