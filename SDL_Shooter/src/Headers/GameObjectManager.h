#pragma once

#include "GameObject.h"
#include "RenderManager.h"
#include "CollisionManager.h"
#include <string>
#include <vector>
#include <map>

class GameObjectManager
{
public:
	GameObjectManager();
	~GameObjectManager();

	void Initialise();
	void Update();
	void Render();

	static void AddGameObject(GameObject* gameObject);
	static GameObject* FindGameObjectWithName(std::string name);
	static void DeleteGameObject(GameObject* gameObject);
	static void DeleteGameObject(GameObject* gameObject, float timeToDelete);

	static void ClearLevel();

private:
	static void InitialiseGameObject(GameObject* gameObject);
	void CheckGameObjectsToDelete();

	static std::vector<GameObject*> gameObjects;
	static std::map<GameObject*, float> gameObjectsToDelete;
	RenderManager* renderManager;
	static CollisionManager* collisionManager;
};