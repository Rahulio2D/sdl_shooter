#pragma once

#include "TextureRenderer.h"
#include "AnimationFrame.h"
#include <vector>

class Animation
{
public:
	Animation(std::vector<AnimationFrame*> animationFrames, float timePerFrame, bool loopAnimation = true);
	~Animation();

	void Update(TextureRenderer* textureRenderer);
	void Reset();

private:
	std::vector<AnimationFrame*> animationFrames;

	int currentFrame;
	float timeElapsed;
	float timePerFrame;
	bool loopAnimation;
};