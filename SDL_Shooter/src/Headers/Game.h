#pragma once

#include "SDL.h"
#include "SDL_image.h"
#include "Camera.h"
#include "Input.h"
#include "Texture.h"
#include "GameObject.h"
#include "AssetManager.h"
#include "GameObjectManager.h"
#include "LevelEditor.h"

class Game
{
public:
	Game(const char* title, int xPos, int yPos, int width, int height, bool fullscreen);
	~Game();

	void Initialise();
	void Update();
	void HandleEvents();
	void Render();
	bool IsRunning() { return isRunning; }
	static void Quit() { isRunning = false; }

	static const std::string GET_SCENE() { return currentScene; }
	static SDL_Renderer* GET_RENDERER() { return renderer; }
	static unsigned int WIDTH;
	static unsigned int HEIGHT;

private:
	int InitialiseSDL(const char* title, int xPos, int yPos, int width, int height, bool fullscreen);
	int CreateWindow(const char* title, int xPos, int yPos, int width, int height, bool fullscreen);
	int CreateRenderer();

	void InitialiseGame();
	void InitialiseLevelEditor();

	static bool isRunning;
	static bool isMainMenu;
	static bool isLevelEditor;
	static std::string currentScene;

	SDL_Window* window;
	Camera* camera;
	Input* input;
	AssetManager* textureManager;
	GameObjectManager* gameObjectManager;
	LevelEditor* levelEditor;
	static SDL_Renderer* renderer;
};