#pragma once

#include "Transform.h"
#include "BoxCollider.h"
#include "PhysicsSystem.h"
#include <string>

class GameObject
{
public:
  GameObject();
  virtual ~GameObject();
  virtual void Initialise() = 0;
  virtual void Update() = 0;
  virtual void Render() = 0;

  Transform* transform;
  BoxCollider* boxCollider;
  PhysicsSystem* physicsSystem;
  std::string name;
};