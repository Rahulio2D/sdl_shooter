#pragma once

#include "UI/UIImage.h"
#include "UI/UIText.h"
#include <unordered_map>

class UIManager
{
public:
	UIManager();
	~UIManager();

	static UIText* CreateText(UIText* text, std::string name);
	static UIImage* CreateImage(UIImage* image, std::string name);

	static UIText* GetText(std::string name);
	static UIImage* GetImage(std::string name);

	static bool RemoveText(std::string name, UIText* text);
	static bool RemoveImage(std::string name, UIImage* image);

	static void Render();

private:
	static std::unordered_map<std::string, UIText*> textElements;
	static std::unordered_map<std::string, UIImage*> imageElements;
};