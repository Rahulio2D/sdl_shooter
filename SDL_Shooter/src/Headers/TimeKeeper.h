#pragma once

#include <SDL.h>

class TimeKeeper
{
public:
  static void CalculateDeltaTime();
  static float GetDeltaTime() { return deltaTime; }

private:
  static float deltaTime;

  static uint64_t ticksPreviousFrame;
  static uint64_t ticksThisFrame;
};