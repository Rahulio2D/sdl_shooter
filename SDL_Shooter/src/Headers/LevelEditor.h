#pragma once

#include <vector>
#include <tinyxml2.h>
#include "Vector2.h"
class GameObject;

struct GameObjectData
{
	std::string type;
	Vector2i position;
	GameObject* gameObject;
};

class LevelEditor
{
public:
	LevelEditor();
	~LevelEditor();
	void Update();
	void Render();
	
private:
	void RenderLevelGrid();
	void RenderGameObjects();
	void SaveLevelToXML();
	void AddToXMLDocument(tinyxml2::XMLDocument& document, tinyxml2::XMLNode* rootNode, GameObjectData& data);

	void InitialiseChoosableGameObjects();
	void SwitchObjectOnKeyPress();
	void AddGameObjectOnClick();
	void RemoveGameObjectOnClick();
	void RemoveGameObjectAtPosition(Vector2i position, std::string type);
	void ZoomCamera();
	void PanCamera();
	GameObjectData* GetGameObjectData(Vector2i position);

	std::string GetWallType(Vector2i position);
	void UpdateNeighbourWalls(Vector2i position);
	std::string CurrentGameObjectType();
	void InitialisePlayerObject();

	void RemoveGameObject(Vector2i position);
	GameObjectData AddGameObject(Vector2i position, std::string type);
	void SaveGameObjectData(GameObjectData data);

	GameObject* GameObjectFromType(std::string type);
	Vector2f mouseHeldDownPosition;
	#define GAMEOBJECT_LIST_LAYERS 10
	std::vector<GameObjectData> gameObjectList[GAMEOBJECT_LIST_LAYERS];			// The list of gameobjects to populate the XML file with
	std::vector<GameObjectData> enemiesList;																// The list of enemies to populate the XML file with
	GameObjectData playerObject;
	std::vector<GameObjectData> choosableGameObjects;		// List of gameobjects which can be picked by designer
	unsigned int currentChosenObject;										// The current chosen object (i.e. index)

	unsigned int GetHashIndex(Vector2i position);
};