#pragma once

#include "SDL.h"
#include "Vector2.h"
#include <vector>

class Input
{
public:
	Input();
	~Input();

	void HandleInput();

	static bool GetKey(SDL_Keycode key);			// Key is held down
	static bool GetKeyDown(SDL_Keycode key);	// Key was pressed this frame
	static bool GetKeyUp(SDL_Keycode key);		// Key was released this frame

	static bool GetMouseButton(int button);
	static bool GetMouseButtonDown(int button);
	static bool GetMouseButtonUp(int button);

	static float GetMouseScrollX() { return mouseScrollX; }
	static float GetMouseScrollY() { return mouseScrollY; }

	static Vector2i GetMousePosition() { return Vector2i(mouseX, mouseY); }

private:
	void ClearInput();

	void AddKeyPressed(SDL_Keycode key);
	void AddKeyReleased(SDL_Keycode key);

	void AddMouseButtonPressed(int button);
	void AddMouseButtonReleased(int button);
	void SetMousePosition();
	void SetMouseScrollWheel(SDL_MouseWheelEvent event);

	static std::vector<SDL_Keycode> keysCurrentlyHeld;
	static std::vector<SDL_Keycode> keysPressedThisFrame;
	static std::vector<SDL_Keycode> keysReleasedThisFrame;

	static bool mouseButtonsCurrentlyHeld[3];
	static bool mouseButtonsPressedThisFrame[3];
	static bool mouseButtonsReleasedThisFrame[3];

	static int mouseX, mouseY;
	static float mouseScrollX, mouseScrollY;

	static int AddUniqueKey(std::vector<SDL_Keycode>* keysVector, SDL_Keycode key);
	static bool FoundKey(std::vector<SDL_Keycode>* keysVector, SDL_Keycode key);
	static bool ValidMouseButton(int button) { return (button >= 0 && button < 3); }
};
