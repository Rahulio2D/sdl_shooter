#pragma once

#include "SDL_ttf.h"

class Font
{
public:
  Font(const char* fontPath, uint16_t fontSize);
  ~Font();

  TTF_Font* GetFont() const { return font; }

private:
  TTF_Font* font;
};