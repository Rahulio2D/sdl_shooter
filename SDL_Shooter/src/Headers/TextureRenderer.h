#pragma once

#include "Texture.h"
#include "Transform.h"
#include "SDL.h"

class TextureRenderer
{
public:
  TextureRenderer(Texture* texture, Transform* transform, SDL_Rect sourceRect, unsigned int layerOrder = 0);
  ~TextureRenderer();
  void Render();
  void SetSourceRect(SDL_Rect sourceRect);

  SDL_Rect* GetSourceRect() { return &sourceRect; }
  SDL_Rect* GetDestinationRect();

  unsigned int layerOrder;
  bool flipX;
  bool flipY;

  Transform* transform;
  Texture* texture;

private:
  SDL_Rect sourceRect;
  SDL_Rect destinationRect;

  void CalculateDestinationRect();
  SDL_RendererFlip TextureFlipState();
};