#pragma once

class MathHelper
{
public:
	static int Clamp(int value, int minimum, int maximum)
	{
		if(value <= maximum && value >= minimum) return value;
		if(value < minimum) return minimum;
		return maximum;
	}

	/// <summary>
	/// Smoothly lerp between startValue and endValue
	/// </summary>
	/// <param name="startValue">The starting value</param>
	/// <param name="endValue">The ending value</param>
	/// <param name="t">The rate of time (between 0.0 and 1.0)</param>
	/// <returns></returns>
	static float Lerp(float startValue, float endValue, float t)
	{
		if(t <= 0.0f) return startValue;
		if(t >= 1.0f) return endValue;
		return startValue + ((endValue - startValue) * t);
	}
};