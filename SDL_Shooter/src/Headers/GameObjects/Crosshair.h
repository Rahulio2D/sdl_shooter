#pragma once

#include "GameObject.h"
#include "TextureRenderer.h"

class Crosshair : public GameObject
{
public:
  Crosshair();
  ~Crosshair();
  void Initialise();
  void Update();
  void Render();

private:
  Texture* texture;
  TextureRenderer* textureRenderer;
};
