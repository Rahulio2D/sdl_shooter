#pragma once

#include "GameObject.h"
#include "TextureRenderer.h"
#include "Texture.h"

class FloorTile : public GameObject
{
public:
  FloorTile();
  ~FloorTile();
  void Initialise();
  void Update();
  void Render();

private:
  Texture* texture;
  TextureRenderer* textureRenderer;
};
