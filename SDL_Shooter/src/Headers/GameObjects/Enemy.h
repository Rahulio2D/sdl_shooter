#pragma once

#include "GameObject.h"
#include "TextureRenderer.h"

class Enemy : public GameObject
{
public:
  Enemy();
  ~Enemy();
  void Initialise();
  void Update();
  void Render();

private:
  Vector2f MoveVelocity();
  Texture* texture;
  TextureRenderer* textureRenderer;

  GameObject* player;

  float moveSpeed;
};