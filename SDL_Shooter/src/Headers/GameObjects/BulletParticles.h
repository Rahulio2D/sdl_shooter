#pragma once

#include "GameObject.h"
#include "ParticleSystem.h"

class BulletParticles : public GameObject
{
public:
  BulletParticles();
  ~BulletParticles();
  void Initialise();
  void Update();
  void Render();

private:
  ParticleSystem* particleSystem;
};