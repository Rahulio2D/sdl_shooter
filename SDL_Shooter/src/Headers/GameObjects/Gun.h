#pragma once

#include "GameObject.h"
#include "Texture.h"
#include "TextureRenderer.h"
#include <unordered_map>

class UIImage;
class UIText;

struct GunData
{
  std::string name;             // The name of this gun
  Texture* texture;             // The texture for this gun
  SDL_Rect sourceRect;          // Source rect for the texture of this gun
  Vector2f offset;              // The offset of this gun compared to the player's position
  Vector2f size;                // The transform size of this gun
  float fireRate;               // The time between each bullet
  unsigned int bulletsPerMag;   // Bullets per magazine
  unsigned int numberOfMags;    // Total number of magazines
  bool fullAuto;                // Should the gun fire when mouse held (true) or mouse clicked (false)
  unsigned int bulletsPerShot;  // How many bullets should be fired per shot
  float timeBetweenBullets;     // Time between bullets per shot
  float reloadTime;             // The time required to reload this gun
};

enum class GunType
{
  AK47, AUG_A3, FN_SCAR, P90, THOMPSON
};

class Gun : public GameObject
{
public:
  Gun();
  ~Gun();
  void Initialise();
  void Update();
  void Render();

  void SetGun(GunType gun);

private:
  void InitialiseGuns();
  void CalculateGunAngle();
  void ShootBullet();
  void HandleBulletLogic();
  void SetupNewGun();
  void ReloadWeapon();
  void HandleReloadLogic();
  void InitialiseUIElements();
  void SetWeaponText();
  void SetAmmoText();

  Texture* texture;
  TextureRenderer* textureRenderer;

  GameObject* player;

  std::unordered_map<GunType, GunData> listOfGuns;
  GunData* currentGun;
  
  bool isShooting;
  int bulletsShot;
  float timeToNextShot;
  float timeToNextBullet;

  unsigned int remainingBulletsThisMag;
  unsigned int totalRemainingBullets;
  float reloadTimeRemaining;
  bool isReloading;

  // UI Elements
  UIImage* backgroundImage;
  UIText* weaponText;
  UIText* ammoText;
};