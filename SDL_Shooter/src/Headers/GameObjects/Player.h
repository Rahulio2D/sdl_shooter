#pragma once

#include "GameObject.h"
#include "TextureRenderer.h"
#include "Texture.h"
#include "BoxCollider.h"
#include "Animator.h"

struct CameraBoundingBox
{
  Vector2f position;
  Vector2f size;
};

class Player : public GameObject
{
public:
  Player();
  ~Player();
  void Initialise();
  void Update();
  void Render();

private:
  void UpdateBoundingBoxPosition();
  void CalculateMoveVelocity();
  void CalculateDashVelocity();
  void BeginDash();

  Animator* animator;
  Animation* idleAnimation;
  Animation* runAnimation;
  void InitialiseAnimations();
  void UpdateCurrentAnimation();

  Vector2f InputDirection();
  Texture* texture;
  TextureRenderer* textureRenderer;

  Vector2f velocity;
  float maxMoveSpeed;

  float dashSpeed;
  float maxDashTime, dashTimeRemaining;
  float dashCooldown, timeToNextDash;
  Vector2f dashDirection;
  bool isDashing;

  Vector2f inputDirectionLastFrame;

  GameObject* gun;
  GameObject* crosshair;
  CameraBoundingBox boundingBox;
};
