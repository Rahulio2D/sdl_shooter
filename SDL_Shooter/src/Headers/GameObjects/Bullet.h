#pragma once

#include "GameObject.h"
#include "Texture.h"
#include "TextureRenderer.h"

class Bullet : public GameObject
{
public:
  Bullet();
  ~Bullet();
  void Initialise();
  void Update();
  void Render();

private:
  void CollisionEffect();

  Vector2f moveVelocity;
  float moveSpeed;
  float timeToDestroy;

  Texture* texture;
  TextureRenderer* textureRenderer;
};