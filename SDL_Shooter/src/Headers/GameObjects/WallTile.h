#pragma once

#include "GameObject.h"
#include "TextureRenderer.h"
#include "Texture.h"
#include "BoxCollider.h"

enum class WallTileType { TOP, BOTTOM, LEFT, RIGHT, TOPLEFT, TOPRIGHT, BOTTOMLEFT, BOTTOMRIGHT, LEFTCORNER, RIGHTCORNER };

class WallTile : public GameObject
{
public:
  WallTile(WallTileType tileType = WallTileType::TOP);
  ~WallTile();
  void Initialise();
  void Update();
  void Render();


private:
  Texture* texture;
  TextureRenderer* textureRenderer;

  SDL_Rect GetRectBasedOnType(WallTileType tileType);
};
