#pragma once

#include "SDL.h"
#include "Vector2.h"
#include "Transform.h"

struct Camera
{
  Camera(float orthographicSize);
  static void SetOrthographicSize(float newOrthographicSize);

  static float GetOrthographicSize() { return orthographicSize; }
  static float GetUnitSize() { return unitSize; }

  static Vector2f ScreenToWorldUnits(Vector2i screenPosition);
  static Vector2i WorldToScreenCoordinates(Vector2f worldPosition);

  static SDL_Rect GetDestinationRect(Vector2f position, Vector2f size);
  
  static Transform* transform;

private:
  static Vector2i center;
  static float orthographicSize;
  static float unitSize;
};
