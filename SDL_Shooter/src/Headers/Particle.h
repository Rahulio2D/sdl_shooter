#pragma once

#include "Transform.h"
#include "TextureRenderer.h"

struct Particle
{
	Transform* transform;
	Vector2f velocity;
	TextureRenderer* textureRenderer;
};