#pragma once

#include "TextureRenderer.h"
#include "Animation.h"
#include <map>

class Animator
{
public:
	Animator(TextureRenderer* textureRenderer);
	~Animator();

	void AddAnimation(std::string animationName, Animation* animation);
	void SetAnimation(std::string animationName);
	void Play();

private:
	Animation* GetAnimationIfExists(std::string animationName);

	std::map<std::string, Animation*> animations;
	std::string currentAnimationName;
	Animation* currentAnimation;

	TextureRenderer* textureRenderer;
};