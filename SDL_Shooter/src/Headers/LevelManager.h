#pragma once

#include "Vector2.h"
#include <string>
#include <vector>
class GameObject;

struct BasicObjectData
{
	std::string type;
	Vector2f position;
};

class LevelManager
{
public:
	static std::vector<BasicObjectData> LoadLevelData(std::string levelPath);
	static void LoadLevelObjects(std::string levelPath);
	static void UnloadLevel();

private:
	static void LoadDataFromXmlFile(std::string levelPath);
	static GameObject* GameObjectType(std::string type);
	static int GameObjectPosition(std::string positionString, Vector2f& position);
	static std::vector<BasicObjectData> gameObjectList;
};