#pragma once

#include <string>
#include <vector>
#include "AssetManager.h"
#include "TextureRenderer.h"
#include "Transform.h"
#include "Particle.h"

class ParticleSystem
{
public:
	ParticleSystem(Transform* transform, std::string texturePath, unsigned int particleCount, float particleLifetime);
	~ParticleSystem();

	void Update();
	void Render();

	float speed;

	Vector2f startPosition;

	Vector2f startSize;
	Vector2f endSize;

	float startRotation;
	float endRotation;

	bool loopEffect;

	unsigned int layerOrder;

private:
	Transform* transform;
	Texture* texture;
	std::vector<Particle*> particles;
	unsigned int particleCount;
	float particleLifetime;
	float particleTimeRemaining;

	void DeleteParticles();
};