#pragma once

#include "Vector2.h"

struct Transform 
{
  Vector2f position;
  Vector2f size;
  float rotation;

  Transform();
};