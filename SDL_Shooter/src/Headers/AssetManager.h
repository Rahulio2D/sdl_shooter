#pragma once

#include "Texture.h"
#include "Font.h"
#include <string>
#include <map>

class AssetManager
{
public:
	AssetManager();
	~AssetManager();

	static Texture* GetTexture(std::string texturePath);
	static Font* GetFont(std::string fontPath, uint16_t fontSize);

private:
	static std::map<std::string, Texture*> textures;
	static std::map<std::string, Font*> fonts;

	static Texture* CreateTexture(std::string texturePath);
	static Font* CreateFont(std::string fontPath, uint16_t fontSize);
};