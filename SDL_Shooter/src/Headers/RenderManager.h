#pragma once

#include "TextureRenderer.h"
#include <vector>

class RenderManager
{
public:
	RenderManager();
	~RenderManager();

	static void AddToQueue(TextureRenderer* textureRenderer);
	static void Render();

private:
	static void ClearQueue();
	static std::vector<TextureRenderer*> listOfTextures;
};