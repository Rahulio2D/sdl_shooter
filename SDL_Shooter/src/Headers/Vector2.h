#pragma once

#include <ostream>
#include "MathHelper.h"

template<typename T>
struct Vector2
{
  T x, y;

  Vector2()
  {
    x = y = 0;
  }

  Vector2(T x, T y)
  {
    this->x = x;
    this->y = y;
  }

  Vector2 operator/(float a)
  {
    return Vector2(x / a, y / a);
  }

  void operator/=(float a)
  {
    x /= a;
    y /= a;
  }

  Vector2 operator*(float a)
  {
    return Vector2(x * a, y * a);
  }

  void operator*=(float a)
  {
    x *= a;
    y *= a;
  }

  Vector2 operator*(Vector2 v)
  {
    return Vector2(x * v.x, y * v.y);
  }

  void operator*=(Vector2 v)
  {
    x *= v.x;
    y *= v.y;
  }

  Vector2 operator/(Vector2 v)
  {
    return Vector2(x / v.x, y / v.y);
  }

  void operator/=(Vector2 v)
  {
    x /= v.x;
    y /= v.y;
  }

  Vector2 operator-(Vector2 v)
  {
    return Vector2(x - v.x, y - v.y);
  }

  void operator-=(Vector2 v)
  {
    x -= v.x;
    y -= v.y;
  }

  Vector2 operator+(Vector2 v)
  {
    return Vector2(x + v.x, y + v.y);
  }

  void operator+=(Vector2 v)
  {
    x += v.x;
    y += v.y;
  }

  bool operator==(Vector2 v) const
  {
    return x == v.x && y == v.y;
  }

  bool operator!=(Vector2 v) const
  {
    return x != v.x || y != v.y;
  }

  void Normalise()
  {
    float magnitude = Magnitude();
    x *= magnitude;
    y *= magnitude;
  }

  // This is built with inspiration from Quake III's Fast Inverse Square Root solution
  // https://www.youtube.com/watch?v=p8u_k2LIZyo
  float Magnitude() const
  {
    float sum = (x * x) + (y * y);
    float halfSum = sum / 2.0f;
    long i = *(long*)&sum;
    i = 0x5f3759df - (i >> 1);
    sum = *(float*)&i;
    sum *= 1.5f - (halfSum * sum * sum);
    return sum;
  }

  static Vector2 Lerp(Vector2 start, Vector2 end, float t)
  {
    Vector2 v;
    v.x = MathHelper::Lerp(start.x, end.x, t);
    v.y = MathHelper::Lerp(start.y, end.y, t);
    return v;
  }


  friend std::ostream& operator<<(std::ostream& os, Vector2 v)
  {
    os << "(" << v.x << ", " << v.y << ")";
    return os;
  }
};

typedef Vector2<float> Vector2f;
typedef Vector2<int> Vector2i;
