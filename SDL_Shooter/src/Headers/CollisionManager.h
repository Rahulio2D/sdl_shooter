#pragma once

#include "BoxCollider.h"
#include "Vector2.h"
#include <vector>

class CollisionManager
{
public:
	CollisionManager(Vector2i levelSize);
	~CollisionManager();

	void AddCollider(BoxCollider* boxCollider);
	void RemoveCollider(BoxCollider* boxCollider);
	void UpdateCollisionMatrix();
	bool CheckCollision(BoxCollider* boxCollider, Vector2f& velocity, Vector2f& collisionPoint);
	void RenderCollisionMatrix();

	static CollisionManager* Instance;

private:
	Vector2i GetColliderPosition(BoxCollider* boxCollider);
	void CollisionCorrection(BoxCollider* first, BoxCollider* second, Vector2f& velocity);
	bool AreCollidersIntersecting(BoxCollider* first, BoxCollider* second);
	bool IsPointInCollider(Vector2f point, BoxCollider* collider);

	void SetCorners(BoxCollider* collider, Vector2f& topRight, Vector2f& bottomLeft, Vector2f& topLeft, Vector2f& bottomRight);
	void ShrinkCornersHorizontal(Vector2f& topRight, Vector2f& bottomLeft, Vector2f& topLeft, Vector2f& bottomRight);
	void ShrinkCornersVertical(Vector2f& topRight, Vector2f& bottomLeft, Vector2f& topLeft, Vector2f& bottomRight);

	#define NUMBER_OF_UNITS 7
	std::vector<BoxCollider*> boxColliders;
	std::vector<int> colliders[NUMBER_OF_UNITS][NUMBER_OF_UNITS];
	Vector2i levelSize;
	Vector2f unitSize;
};