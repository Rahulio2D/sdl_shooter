#pragma once

#include "Transform.h"
#include "Vector2.h"

struct BoxCollider
{
public:
  BoxCollider(Transform* transform, Vector2f offset = Vector2f(), Vector2f size = Vector2f(1, 1));
  ~BoxCollider();

  Vector2f GetPosition() { return transform->position + offset; }
  Vector2f GetSize() { return transform->size * size; }

  Vector2f GetBottomLeft() { return GetPosition() - GetSize() / 2.0f; }
  Vector2f GetTopRight() { return GetPosition() + GetSize() / 2.0f; }

  Transform* transform;
  Vector2f offset;
  Vector2f size;
};