#pragma once

#include "Texture.h"

struct AnimationFrame
{
	Texture* texture;
	SDL_Rect sourceRect;
};