#pragma once

#include "BoxCollider.h"
#include "Vector2.h"

class PhysicsSystem
{
public:
	PhysicsSystem(BoxCollider* boxCollider);
	~PhysicsSystem();
	void Update();

	void CheckCollisions();
	bool GetIsColliding() const { return isColliding; }

	void SetCollisionPoint(const Vector2f point) { collisionPoint = point; }
	Vector2f GetCollisionPoint() const { return collisionPoint; }

	Vector2f velocity;

private:
	BoxCollider* boxCollider;
	Vector2f collisionPoint;
	bool isColliding;
};