#include "GameObject.h"

GameObject::GameObject()
{
  transform = new Transform();
  boxCollider = nullptr;
  physicsSystem = nullptr;
}

GameObject::~GameObject()
{
  delete transform;
  if(boxCollider != nullptr) delete boxCollider;
  if(physicsSystem != nullptr) delete physicsSystem;
}
