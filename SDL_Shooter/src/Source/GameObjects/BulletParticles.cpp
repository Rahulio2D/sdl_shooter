#include "GameObjects/BulletParticles.h"
#include "GameObjectManager.h"

BulletParticles::BulletParticles()
{
  this->name = "BulletParticles";
}

BulletParticles::~BulletParticles()
{
  delete particleSystem;
}

void BulletParticles::Initialise()
{
  particleSystem = new ParticleSystem(transform, "Assets/white.png", 12, 0.5f);

  particleSystem->endRotation = 90.0f;
  particleSystem->startSize = Vector2f(0.3f, 0.3f);
  particleSystem->endSize = Vector2f(0, 0);
  particleSystem->speed = 1.5f;
  particleSystem->layerOrder = 2;

  GameObjectManager::DeleteGameObject(this, 2.0f);
}

void BulletParticles::Update()
{
  particleSystem->Update();
}

void BulletParticles::Render()
{
  particleSystem->Render();
}
