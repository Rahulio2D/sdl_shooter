#include "GameObjects/FloorTile.h"
#include "Game.h"

FloorTile::FloorTile()
{
  texture = AssetManager::GetTexture("Assets/Tileset.png");
  SDL_Rect sourceRect = SDL_Rect{32, 32, 32, 32};
  textureRenderer = new TextureRenderer(texture, transform, sourceRect);
  textureRenderer->layerOrder = 1;
}

FloorTile::~FloorTile()
{
  delete textureRenderer;
  texture = nullptr;
}

void FloorTile::Initialise()
{
}

void FloorTile::Update()
{
  // Not needed
}

void FloorTile::Render()
{
  RenderManager::AddToQueue(textureRenderer);
}
