#include "GameObjects/Player.h"
#include "GameObjects/Crosshair.h"
#include "GameObjects/Gun.h"
#include "GameObjectManager.h"
#include "AssetManager.h"
#include "TimeKeeper.h"
#include "Camera.h"
#include "Input.h"

Player::Player()
{
  this->name = "Player";
  texture = AssetManager::GetTexture("Assets/Adventurer Sprite Sheet.png");
  SDL_Rect sourceRect = SDL_Rect{ 0, 0, 32, 32 };
  textureRenderer = new TextureRenderer(texture, transform, sourceRect);
  animator = new Animator(textureRenderer);
  textureRenderer->layerOrder = 3;

  InitialiseAnimations();

  transform->position = Vector2f(0, 0);
  boxCollider = new BoxCollider(transform, Vector2f(), Vector2f(0.5, 0.5));
  physicsSystem = new PhysicsSystem(boxCollider);

  inputDirectionLastFrame = Vector2f(0, 0);
  maxMoveSpeed = 5.0f;
  velocity = Vector2f();

  dashSpeed = 27.0f;
  dashTimeRemaining = 0.0f;
  maxDashTime = 0.13f;
  dashCooldown = 0.5f;
  timeToNextDash = 0.0f;
  dashDirection = Vector2f();
  isDashing = false;

  gun = nullptr;
  crosshair = nullptr;

  boundingBox = CameraBoundingBox({Vector2f(0, 0), Vector2f(10, 6)});
}

Player::~Player()
{
  delete animator;
  delete textureRenderer;
  texture = nullptr;
  if(gun != nullptr) GameObjectManager::DeleteGameObject(gun);
  if(crosshair != nullptr) GameObjectManager::DeleteGameObject(crosshair);
}

void Player::Initialise()
{
  gun = new Gun();
  crosshair = new Crosshair();
  gun->transform->position = crosshair->transform->position = this->transform->position;
  GameObjectManager::AddGameObject(gun);
  GameObjectManager::AddGameObject(crosshair);
}

void Player::Update()
{
  if(Input::GetKeyDown(SDLK_LSHIFT)) BeginDash();

  if(isDashing) CalculateDashVelocity();
  else CalculateMoveVelocity();

  UpdateCurrentAnimation();

  physicsSystem->velocity = velocity;
  physicsSystem->Update();

  UpdateBoundingBoxPosition();
}

void Player::Render()
{
  RenderManager::AddToQueue(textureRenderer);
}

void Player::UpdateBoundingBoxPosition()
{
  Vector2f topRight = boxCollider->GetTopRight();
  Vector2f bottomLeft = boxCollider->GetBottomLeft();
  Vector2f boundingBoxTopRight = boundingBox.position + (boundingBox.size / 2.0f);
  Vector2f boundingBoxBottomLeft = boundingBox.position - (boundingBox.size / 2.0f);

  if(topRight.x > boundingBoxTopRight.x) boundingBox.position.x = topRight.x - (boundingBox.size.x / 2.0f);
  else if(bottomLeft.x < boundingBoxBottomLeft.x) boundingBox.position.x = bottomLeft.x + (boundingBox.size.x / 2.0f);

  if(topRight.y > boundingBoxTopRight.y) boundingBox.position.y = topRight.y - (boundingBox.size.y / 2.0f);
  else if(bottomLeft.y < boundingBoxBottomLeft.y) boundingBox.position.y = bottomLeft.y + (boundingBox.size.y / 2.0f);

  Camera::transform->position = boundingBox.position;
}

void Player::CalculateMoveVelocity()
{
  Vector2f inputDirection = InputDirection();

  if (inputDirectionLastFrame != Vector2f(0, 0))
  {
    if (inputDirectionLastFrame.y != 0) inputDirection.x = 0;
    else if (inputDirectionLastFrame.x != 0) inputDirection.y = 0;
  }

  inputDirectionLastFrame = inputDirection;
  velocity = inputDirection * maxMoveSpeed;
  timeToNextDash -= TimeKeeper::GetDeltaTime();
}

void Player::CalculateDashVelocity()
{
  velocity = dashDirection * dashSpeed;
  dashTimeRemaining -= TimeKeeper::GetDeltaTime();
  if(dashTimeRemaining <= 0.0f)
  {
    isDashing = false;
    timeToNextDash = dashCooldown;
  }
}

void Player::BeginDash()
{
  if(isDashing || timeToNextDash > 0.0f) return;
  
  dashDirection = inputDirectionLastFrame;
  dashTimeRemaining = maxDashTime;

  isDashing = true;
}

void Player::InitialiseAnimations()
{
  std::vector<AnimationFrame*> idleFrames;
  for(int i = 0; i < 13; i++)
  {
    SDL_Rect sourceRect{32 * i, 0, 32, 32};
    idleFrames.push_back(new AnimationFrame{texture, sourceRect});
  }
  idleAnimation = new Animation(idleFrames, 0.1f);

  std::vector<AnimationFrame*> runFrames;
  for(int i = 0; i < 8; i++)
  {
    SDL_Rect sourceRect{32 * i, 32, 32, 32};
    runFrames.push_back(new AnimationFrame{texture, sourceRect});
  }
  runAnimation = new Animation(runFrames, 0.1f);

  animator->AddAnimation("Idle", idleAnimation);
  animator->AddAnimation("Run", runAnimation);
  animator->SetAnimation("Idle");
}

void Player::UpdateCurrentAnimation()
{
  Vector2f inputDirection = InputDirection();
  if(inputDirection != Vector2f(0, 0))
  {
    textureRenderer->flipX = InputDirection().x < 0;
    animator->SetAnimation("Run");
  }
  else
  {
    animator->SetAnimation("Idle");
  }
  animator->Play();
}

Vector2f Player::InputDirection()
{
  Vector2f inputDirection(0, 0);
  if (Input::GetKey(SDLK_s)) inputDirection.y += -1;
  if (Input::GetKey(SDLK_w)) inputDirection.y += 1;
  if (Input::GetKey(SDLK_a)) inputDirection.x += -1;
  if (Input::GetKey(SDLK_d)) inputDirection.x += 1;

  return inputDirection;
}
