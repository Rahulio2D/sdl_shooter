#include "GameObjects/Gun.h"
#include "GameObjects/Bullet.h"
#include "GameObjectManager.h"
#include "AssetManager.h"
#include "Camera.h"
#include "Input.h"
#include "TimeKeeper.h"
#include "UIManager.h"
#include "Game.h"

Gun::Gun()
{
  this->name = "Gun";
  texture = AssetManager::GetTexture("Assets/weapons/ak47.png");
  SDL_Rect sourceRect = SDL_Rect{ 0, 0, 110, 35};
  textureRenderer = new TextureRenderer(texture, transform, sourceRect);
  textureRenderer->layerOrder = 5;
  
  currentGun = nullptr;

  isShooting = false;
  bulletsShot = 0;
  timeToNextShot = 0.0f;
  timeToNextBullet = 0.0f;

  timeToNextShot = 0.0f;
  remainingBulletsThisMag = 0;
  totalRemainingBullets = 0;
  reloadTimeRemaining = 0.0f;
  isReloading = false;

  transform->size = Vector2f(0.7f, 0.35f);
  player = nullptr;

  backgroundImage = nullptr;
  weaponText = nullptr;
  ammoText = nullptr;
}

Gun::~Gun()
{
  delete textureRenderer;
  UIManager::RemoveText("Ammo Text", ammoText);
  UIManager::RemoveText("Weapon Text", weaponText);
  UIManager::RemoveImage("Ammo Background", backgroundImage);
  texture = nullptr;
  player = nullptr;
  currentGun = nullptr;
  listOfGuns.clear();
}

void Gun::Initialise()
{
  player = GameObjectManager::FindGameObjectWithName("Player");
  if(Game::GET_SCENE() != "LevelEditor") InitialiseUIElements();
  InitialiseGuns();
  SetGun(GunType::AK47);
}

void Gun::Update()
{
  if(player != nullptr) transform->position = player->transform->position - currentGun->offset;
  else player = GameObjectManager::FindGameObjectWithName("Player");

  CalculateGunAngle();

  if((Input::GetMouseButtonDown(0) && !currentGun->fullAuto) || (Input::GetMouseButton(0) && currentGun->fullAuto)) ShootBullet();
  HandleBulletLogic();

  if(Input::GetKeyDown(SDLK_r)) ReloadWeapon();
  if(isReloading) HandleReloadLogic();
}

void Gun::Render()
{
  RenderManager::AddToQueue(textureRenderer);
}

void Gun::SetGun(GunType gunType)
{
  for(auto it = listOfGuns.begin(); it != listOfGuns.end(); ++it)
  {
    if(it->first == gunType)
    {
      currentGun = &it->second;
      SetupNewGun();
      return;
    }
  }
}

void Gun::InitialiseGuns()
{
  GunData ak47;
  ak47.name = "AK47";
  ak47.texture = AssetManager::GetTexture("Assets/weapons/ak47.png");
  ak47.sourceRect = SDL_Rect{0, 0, 110, 35};
  ak47.offset = Vector2f(0.0f, 0.2f);
  ak47.size = Vector2f(0.7f, 0.35f);
  ak47.fireRate = 0.1f;
  ak47.bulletsPerMag = 30;
  ak47.numberOfMags = 5;
  ak47.fullAuto = true;
  ak47.bulletsPerShot = 1;
  ak47.timeBetweenBullets = 0.0f;
  ak47.reloadTime = 1.2f;

  GunData augA3;
  augA3.name = "AUG A3";
  augA3.texture = AssetManager::GetTexture("Assets/weapons/augA3.png");
  augA3.sourceRect = SDL_Rect{0, 0, 94, 39};
  augA3.offset = Vector2f(0.0f, 0.2f);
  augA3.size = Vector2f(0.7f, 0.35f);
  augA3.fireRate = 0.4f;
  augA3.bulletsPerMag = 24;
  augA3.numberOfMags = 6;
  augA3.fullAuto = true;
  augA3.bulletsPerShot = 3;
  augA3.timeBetweenBullets = 0.075f;
  augA3.reloadTime = 0.8f;

  GunData fnScar;
  fnScar.name = "FN-Scar";
  fnScar.texture = AssetManager::GetTexture("Assets/weapons/fnscarH.png");
  fnScar.sourceRect = SDL_Rect{0, 0, 114, 36};
  fnScar.offset = Vector2f(0.0f, 0.2f);
  fnScar.size = Vector2f(0.7f, 0.35f);
  fnScar.fireRate = 0.07f;
  fnScar.bulletsPerMag = 20;
  fnScar.numberOfMags = 4;
  fnScar.fullAuto = true;
  fnScar.bulletsPerShot = 1;
  fnScar.timeBetweenBullets = 0.0f;
  fnScar.reloadTime = 1.5f;

  GunData p90;
  p90.name = "P90";
  p90.texture = AssetManager::GetTexture("Assets/weapons/p90.png");
  p90.sourceRect = SDL_Rect{0, 0, 61, 24};
  p90.offset = Vector2f(0.0f, 0.2f);
  p90.size = Vector2f(0.7f, 0.35f);
  p90.fireRate = 0.08f;
  p90.bulletsPerMag = 45;
  p90.numberOfMags = 5;
  p90.fullAuto = true;
  p90.bulletsPerShot = 1;
  p90.timeBetweenBullets = 0.0f;
  p90.reloadTime = 0.8f;

  GunData thompson;
  thompson.name = "Thompson";
  thompson.texture = AssetManager::GetTexture("Assets/weapons/thompson.png");
  thompson.sourceRect = SDL_Rect{0, 0, 95, 29};
  thompson.offset = Vector2f(0.0f, 0.2f);
  thompson.size = Vector2f(0.7f, 0.35f);
  thompson.fireRate = 0.1f;
  thompson.bulletsPerMag = 18;
  thompson.numberOfMags = 10;
  thompson.fullAuto = false;
  thompson.bulletsPerShot = 1;
  thompson.timeBetweenBullets = 0.0f;
  thompson.reloadTime = 1.8f;

  listOfGuns.insert(std::pair<GunType, GunData>(GunType::AK47, ak47));
  listOfGuns.insert(std::pair<GunType, GunData>(GunType::AUG_A3, augA3));
  listOfGuns.insert(std::pair<GunType, GunData>(GunType::FN_SCAR, fnScar));
  listOfGuns.insert(std::pair<GunType, GunData>(GunType::P90, p90));
  listOfGuns.insert(std::pair<GunType, GunData>(GunType::THOMPSON, thompson));
}

void Gun::CalculateGunAngle()
{
  Vector2f mousePosition = Camera::ScreenToWorldUnits(Input::GetMousePosition());
  float gunAngle = atan2(mousePosition.y - transform->position.y, mousePosition.x - transform->position.x) * 180.0f / (float)M_PI * -1;

  textureRenderer->flipY = abs(gunAngle) > 90.0f;
  transform->rotation = gunAngle;
}

void Gun::ShootBullet()
{
  if(isReloading || isShooting || timeToNextShot > 0.0f || remainingBulletsThisMag == 0) return;

  bulletsShot = 0;
  timeToNextBullet = 0;
  isShooting = true;
}

void Gun::HandleBulletLogic()
{
  if(isShooting)
  {
    timeToNextBullet -= TimeKeeper::GetDeltaTime();
    if(timeToNextBullet <= 0.0f)
    {
      Bullet* bullet = new Bullet();
      Vector2f direction = Camera::ScreenToWorldUnits(Input::GetMousePosition()) - transform->position;
      direction.Normalise();
      bullet->transform->position = transform->position + direction;
      bullet->transform->rotation = transform->rotation;
      bullet->Initialise();
      GameObjectManager::AddGameObject(bullet);

      timeToNextBullet = currentGun->timeBetweenBullets;
      remainingBulletsThisMag--;
      bulletsShot++;

      SetAmmoText();
    }
    if(bulletsShot >= currentGun->bulletsPerShot)
    {
      timeToNextShot = currentGun->fireRate;
      isShooting = false;
    }
  }
  else
  {
    timeToNextShot -= TimeKeeper::GetDeltaTime();
  }
}

void Gun::SetupNewGun()
{
  textureRenderer->texture = currentGun->texture;
  textureRenderer->SetSourceRect(currentGun->sourceRect);
  transform->size = currentGun->size;

  remainingBulletsThisMag = currentGun->bulletsPerMag;
  totalRemainingBullets = currentGun->bulletsPerMag * currentGun->numberOfMags;

  SetAmmoText();
  SetWeaponText();
}

void Gun::ReloadWeapon()
{
  if(remainingBulletsThisMag < currentGun->bulletsPerMag && totalRemainingBullets > 0 && !isReloading && !isShooting)
  {
    reloadTimeRemaining = currentGun->reloadTime;
    isReloading = true;
  }
}

void Gun::HandleReloadLogic()
{
  reloadTimeRemaining -= TimeKeeper::GetDeltaTime();
  ammoText->SetText("RELOADING");

  if(reloadTimeRemaining <= 0.0f)
  {
    unsigned int bulletsToReload = currentGun->bulletsPerMag - remainingBulletsThisMag;
    if(bulletsToReload < totalRemainingBullets)
    {
      remainingBulletsThisMag = currentGun->bulletsPerMag;
      totalRemainingBullets -= bulletsToReload;
    }
    else
    {
      remainingBulletsThisMag += totalRemainingBullets;
      totalRemainingBullets = 0;
    }

    SetAmmoText();
    reloadTimeRemaining = currentGun->reloadTime;
    isReloading = false;
  }
}

void Gun::InitialiseUIElements()
{
  backgroundImage = new UIImage("Assets/grey.png", Vector2i(120, 50), Vector2i(-20, 20), ANCHOR_POINT::BOTTOM_RIGHT, ANCHOR_POINT::BOTTOM_RIGHT);
  weaponText = new UIText("Assets/upheaval/upheavtt.ttf", 26, "Current Gun", Vector2i(-80, 60), ANCHOR_POINT::BOTTOM_RIGHT, ANCHOR_POINT::CENTER);
  ammoText = new UIText("Assets/upheaval/upheavtt.ttf", 18, "Ammo", Vector2i(-80, 40), ANCHOR_POINT::BOTTOM_RIGHT, ANCHOR_POINT::CENTER);
  UIManager::CreateImage(backgroundImage, "Ammo Background");
  UIManager::CreateText(weaponText, "Weapon Text");
  UIManager::CreateText(ammoText, "Ammo Text");
}

void Gun::SetWeaponText()
{
  if(weaponText == nullptr) return;

  weaponText->SetText(currentGun->name);
}

void Gun::SetAmmoText()
{
  if(ammoText == nullptr) return;

  std::string ammoString = std::to_string(remainingBulletsThisMag) + "/" + std::to_string(totalRemainingBullets);
  ammoText->SetText(ammoString);
}
