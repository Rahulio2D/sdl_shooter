#include "GameObjects/Bullet.h"
#include "GameObjects/BulletParticles.h"
#include "GameObjectManager.h"
#include "AssetManager.h"
#include "Camera.h"
#include "Input.h"
#include "TimeKeeper.h"
#include <iostream>

Bullet::Bullet()
{
  this->name = "Bullet";
  this->texture = AssetManager::GetTexture("assets/bullet.png");
  SDL_Rect sourceRect = SDL_Rect{0, 0, 512, 512};
  textureRenderer = new TextureRenderer(texture, transform, sourceRect, 5);

  moveSpeed = 10.0f;
  timeToDestroy = 7.0f;
  moveVelocity = Vector2f(0, 0);

  transform->size = Vector2f(0.5f, 0.6f);
  boxCollider = new BoxCollider(transform);
  physicsSystem = new PhysicsSystem(boxCollider);
}

Bullet::~Bullet()
{
  delete textureRenderer;
  texture = nullptr;
}

void Bullet::Initialise()
{
  Vector2f mousePosition = Camera::ScreenToWorldUnits(Input::GetMousePosition());
  Vector2f direction = mousePosition - transform->position;
  direction.Normalise();
  moveVelocity = direction * moveSpeed;
  physicsSystem->velocity = moveVelocity;
}

void Bullet::Update()
{
  physicsSystem->Update();

  if(physicsSystem->GetIsColliding()) CollisionEffect();

  timeToDestroy -= TimeKeeper::GetDeltaTime();
  if(timeToDestroy <= 0) GameObjectManager::DeleteGameObject(this);
}

void Bullet::Render()
{
  RenderManager::AddToQueue(textureRenderer);
}

void Bullet::CollisionEffect()
{
  BulletParticles* bulletParticles = new BulletParticles();
  bulletParticles->transform->position = physicsSystem->GetCollisionPoint();
  GameObjectManager::AddGameObject(bulletParticles);
  GameObjectManager::DeleteGameObject(this);
}
