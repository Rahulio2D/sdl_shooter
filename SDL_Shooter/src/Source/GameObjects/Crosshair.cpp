#include "GameObjects/Crosshair.h"
#include "AssetManager.h"
#include "RenderManager.h"
#include "Camera.h"
#include "Input.h"

Crosshair::Crosshair()
{
  this->name = "Crosshair";
  texture = AssetManager::GetTexture("Assets/crosshair.png");
  SDL_Rect sourceRect = SDL_Rect{0, 0, 512, 512};
  textureRenderer = new TextureRenderer(texture, transform, sourceRect, 5);

  transform->size = Vector2f(2.2f, 2.2f);
}

Crosshair::~Crosshair()
{
  delete textureRenderer;
  texture = nullptr;
}

void Crosshair::Initialise()
{
}

void Crosshair::Update()
{
  transform->position = Camera::ScreenToWorldUnits(Input::GetMousePosition());
}

void Crosshair::Render()
{
  RenderManager::AddToQueue(textureRenderer);
}
