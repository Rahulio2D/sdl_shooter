#include "GameObjects/Enemy.h"
#include "GameObjectManager.h"
#include "AssetManager.h"

Enemy::Enemy()
{
  this->name = "Enemy";
  texture = AssetManager::GetTexture("Assets/enemy.png");
  SDL_Rect sourceRect = SDL_Rect{0, 0, 512, 512};
  textureRenderer = new TextureRenderer(texture, transform, sourceRect, 3);

  moveSpeed = 2.0f;
  transform->position = Vector2f(5, 0);
  boxCollider = new BoxCollider(transform);
  physicsSystem = new PhysicsSystem(boxCollider);
}

Enemy::~Enemy()
{
  delete textureRenderer;
  texture = nullptr;
}

void Enemy::Initialise()
{
	player = GameObjectManager::FindGameObjectWithName("Player");
}

void Enemy::Update()
{
  physicsSystem->velocity = MoveVelocity();
  physicsSystem->Update();
}

void Enemy::Render()
{
  RenderManager::AddToQueue(textureRenderer);
}

Vector2f Enemy::MoveVelocity()
{
  if(player == nullptr)
  {
    player = GameObjectManager::FindGameObjectWithName("Player");
    return Vector2f();
  }

  Vector2f direction = player->transform->position - transform->position;
  direction.Normalise();
  return direction * moveSpeed;
}
