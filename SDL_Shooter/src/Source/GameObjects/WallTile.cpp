#include "GameObjects/WallTile.h"
#include "Game.h"
#include <iostream>

WallTile::WallTile(WallTileType tileType)
{
  texture = AssetManager::GetTexture("Assets/Tileset.png");
  textureRenderer = new TextureRenderer(texture, transform, GetRectBasedOnType(tileType));
  textureRenderer->layerOrder = 2;

  boxCollider = new BoxCollider(transform, Vector2f(), Vector2f(1.0f, 1.0f));
}

WallTile::~WallTile()
{
  delete textureRenderer;
  texture = nullptr;
}

void WallTile::Initialise()
{
  // Not needed
}

void WallTile::Update()
{
  // Not needed
}

void WallTile::Render()
{
  RenderManager::AddToQueue(textureRenderer);
}

SDL_Rect WallTile::GetRectBasedOnType(WallTileType tileType)
{
  switch(tileType)
  {
    case WallTileType::TOP: return SDL_Rect{32, 0, 32, 32};
    case WallTileType::BOTTOM: return SDL_Rect{32, 64, 32, 32};
    case WallTileType::LEFT: return SDL_Rect{0, 32, 32, 32};
    case WallTileType::RIGHT: return SDL_Rect{64, 32, 32, 32};
    case WallTileType::TOPLEFT: return SDL_Rect{0, 0, 32, 32};
    case WallTileType::TOPRIGHT: return SDL_Rect{64, 0, 32, 32};
    case WallTileType::BOTTOMLEFT: return SDL_Rect{0, 64, 32, 32};
    case WallTileType::BOTTOMRIGHT: return SDL_Rect{64, 64, 32, 32};
    case WallTileType::LEFTCORNER: return SDL_Rect{0, 192, 32, 32};
    case WallTileType::RIGHTCORNER: return SDL_Rect{32, 192, 32, 32};
    default: return SDL_Rect{32, 0, 32, 32};
  }
}
