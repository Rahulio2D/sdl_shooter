#include "PhysicsSystem.h"
#include "CollisionManager.h"
#include "TimeKeeper.h"

PhysicsSystem::PhysicsSystem(BoxCollider* boxCollider)
{
	this->boxCollider = boxCollider;
	isColliding = false;
	collisionPoint = Vector2f();
}

PhysicsSystem::~PhysicsSystem()
{
	this->boxCollider = nullptr;
}

void PhysicsSystem::Update()
{
	CheckCollisions();
	boxCollider->transform->position += velocity * TimeKeeper::GetDeltaTime();
}

void PhysicsSystem::CheckCollisions()
{
	isColliding = CollisionManager::Instance->CheckCollision(boxCollider, velocity, collisionPoint);
}
