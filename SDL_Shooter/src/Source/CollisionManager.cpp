#include "CollisionManager.h"
#include "Game.h"
#include "Camera.h"
#include "MathHelper.h"
#include <iostream>

CollisionManager* CollisionManager::Instance;

CollisionManager::CollisionManager(Vector2i levelSize)
{
	this->levelSize = levelSize;
	unitSize = Vector2f(levelSize.x, levelSize.y) / NUMBER_OF_UNITS;
	for(auto collider : colliders) *collider = std::vector<int>();
	boxColliders = std::vector<BoxCollider*>();

	if(Instance == nullptr) Instance = this;
}

CollisionManager::~CollisionManager()
{
	for(auto collider : colliders) collider->clear();
	for(auto boxCollider : boxColliders) boxCollider = nullptr;
	boxColliders.clear();
	if(Instance == this) Instance = nullptr;
}

void CollisionManager::AddCollider(BoxCollider* boxCollider)
{
	if(boxCollider == nullptr) return;

	boxColliders.push_back(boxCollider);
}

void CollisionManager::RemoveCollider(BoxCollider* boxCollider)
{
	if(boxCollider == nullptr) return;

	for(int i = 0; i < boxColliders.size(); i++)
	{
		if(boxColliders[i] == boxCollider)
		{
			boxColliders[i] = nullptr;
			return;
		}
	}
}

void CollisionManager::UpdateCollisionMatrix()
{
	for(int i = 0; i < NUMBER_OF_UNITS; i++)
	{
		for(int j = 0; j < NUMBER_OF_UNITS; j++)
		{
			colliders[i][j].clear();
		}
	}

	for(int i = 0; i < boxColliders.size(); i++)
	{
		if(boxColliders[i] == nullptr) 
			continue;

		Vector2i index = GetColliderPosition(boxColliders[i]);
		colliders[index.x][index.y].push_back(i);
	}
}

bool CollisionManager::CheckCollision(BoxCollider* boxCollider, Vector2f& velocity, Vector2f& collisionPoint)
{
	Vector2i colliderPosition = GetColliderPosition(boxCollider);
	Vector2i minPosition = Vector2i(MathHelper::Clamp(colliderPosition.x - 1, 0, NUMBER_OF_UNITS - 1), MathHelper::Clamp(colliderPosition.y - 1, 0, NUMBER_OF_UNITS - 1));
	Vector2i maxPosition = Vector2i(MathHelper::Clamp(colliderPosition.x + 1, 0, NUMBER_OF_UNITS - 1), MathHelper::Clamp(colliderPosition.y + 1, 0, NUMBER_OF_UNITS - 1));
	collisionPoint = Vector2f();
	bool hasCollided = false;
	for(int x = minPosition.x; x <= maxPosition.x; x++)
	{
		for(int y = minPosition.y; y <= maxPosition.y; y++)
		{
			for(auto index : colliders[x][y])
			{
				if(boxCollider == boxColliders[index] || boxColliders[index] == nullptr) continue;

				if(AreCollidersIntersecting(boxCollider, boxColliders[index]))
				{
					collisionPoint = (boxCollider->GetPosition() + boxColliders[index]->GetPosition()) / 2.0f;
					CollisionCorrection(boxCollider, boxColliders[index], velocity);
					hasCollided = true;
				}
			}
		}
	}
	return hasCollided;
}

void CollisionManager::RenderCollisionMatrix()
{
	for(int i = 0; i < NUMBER_OF_UNITS; i++)
	{
		for(int j = 0; j < NUMBER_OF_UNITS; j++)
		{
			Vector2f position  (((-levelSize.x / 2.0f) - (unitSize.x / 2.0f)) + (unitSize.x * (i + 1)), (levelSize.y / 2.0f) - (unitSize.y / 2.0f) - (unitSize.y * j));
			SDL_Rect rect = Camera::GetDestinationRect(position, unitSize);
			SDL_SetRenderDrawColor(Game::GET_RENDERER(), 255, 255, 255, 255);
			if(colliders[i][j].size() > 0)
				SDL_SetRenderDrawColor(Game::GET_RENDERER(), 0, 255, 0, 255);
			SDL_RenderDrawRect(Game::GET_RENDERER(), &rect);
		}
	}

	for(int i = 0; i < boxColliders.size(); i++)
	{
		if(boxColliders[i] == nullptr) continue;
		SDL_Rect destRect = Camera::GetDestinationRect(boxColliders[i]->GetPosition(), boxColliders[i]->GetSize());
		SDL_SetRenderDrawColor(Game::GET_RENDERER(), 0, 255, 0, 255);
		SDL_RenderDrawRect(Game::GET_RENDERER(), &destRect);
	}
}

// Returns collider coordinate position on grid
Vector2i CollisionManager::GetColliderPosition(BoxCollider* boxCollider)
{
	Vector2f size (boxCollider->GetSize().x, -boxCollider->GetSize().y);
	Vector2f position = (boxCollider->GetPosition() / unitSize) - size;
	Vector2i positionOnGrid = Vector2i(round(position.x + NUMBER_OF_UNITS / 2.0f), (round(position.y - (NUMBER_OF_UNITS / 2.0f)) * -1));
	positionOnGrid.x = MathHelper::Clamp(positionOnGrid.x, 0, NUMBER_OF_UNITS - 1);
	positionOnGrid.y = MathHelper::Clamp(positionOnGrid.y, 0, NUMBER_OF_UNITS - 1);
	return positionOnGrid;
}

bool CollisionManager::AreCollidersIntersecting(BoxCollider* first, BoxCollider* second)
{
	if(first->GetTopRight().x < second->GetBottomLeft().x) return false;
	if(first->GetBottomLeft().x > second->GetTopRight().x) return false;
	if(first->GetTopRight().y < second->GetBottomLeft().y) return false;
	if(first->GetBottomLeft().y > second->GetTopRight().y) return false;
	return true;
}

bool CollisionManager::IsPointInCollider(Vector2f point, BoxCollider* collider)
{
	if(point.x < collider->GetBottomLeft().x) return false;
	if(point.x > collider->GetTopRight().x) return false;
	if(point.y < collider->GetBottomLeft().y) return false;
	if(point.y > collider->GetTopRight().y) return false;
	return true;
}

void CollisionManager::CollisionCorrection(BoxCollider* first, BoxCollider* second, Vector2f& velocity)
{
	Vector2f topRight, bottomLeft, topLeft, bottomRight;
	SetCorners(first, topRight, bottomLeft, topLeft, bottomRight);

	Vector2f secondTopRight = second->GetTopRight();
	Vector2f secondBottomLeft = second->GetBottomLeft();

	if(velocity.x != 0)
	{
		ShrinkCornersHorizontal(topRight, bottomLeft, topLeft, bottomRight);
		if((IsPointInCollider(topRight, second) || IsPointInCollider(bottomRight, second)) && velocity.x > 0)
		{
			first->transform->position.x -= topRight.x - secondBottomLeft.x;
			velocity.x = 0;
		}
		else if((IsPointInCollider(bottomLeft, second) || IsPointInCollider(topLeft, second)) && velocity.x < 0)
		{
			first->transform->position.x += secondTopRight.x - bottomLeft.x;
			velocity.x = 0;
		}
	}

	SetCorners(first, topRight, bottomLeft, topLeft, bottomRight);	// Reset corners

	if(velocity.y != 0)
	{
		ShrinkCornersVertical(topRight, bottomLeft, topLeft, bottomRight);
		if((IsPointInCollider(topRight, second) || IsPointInCollider(topLeft, second)) && velocity.y > 0)
		{
			first->transform->position.y -= topRight.y - secondBottomLeft.y;
			velocity.y = 0;
		}
		else if((IsPointInCollider(bottomLeft, second) || IsPointInCollider(bottomRight, second)) && velocity.y < 0)
		{
			first->transform->position.y += secondTopRight.y - bottomLeft.y;
			velocity.y = 0;
		}
	}
}

void CollisionManager::SetCorners(BoxCollider* collider, Vector2f& topRight, Vector2f& bottomLeft, Vector2f& topLeft, Vector2f& bottomRight)
{
	topRight = collider->GetTopRight();
	bottomLeft = collider->GetBottomLeft();
	topLeft = Vector2f(bottomLeft.x, topRight.y);
	bottomRight = Vector2f(topRight.x, bottomLeft.y);
}

void CollisionManager::ShrinkCornersHorizontal(Vector2f& topRight, Vector2f& bottomLeft, Vector2f& topLeft, Vector2f& bottomRight)
{
	topRight -= Vector2f(0.0f, 0.01f);
	bottomLeft += Vector2f(0.0f, 0.01f);
	topLeft -= Vector2f(0.0f, 0.01f);
	bottomRight += Vector2f(0.0f, 0.01f);
}

void CollisionManager::ShrinkCornersVertical(Vector2f& topRight, Vector2f& bottomLeft, Vector2f& topLeft, Vector2f& bottomRight)
{
	topRight -= Vector2f(0.01f, 0.0f);
	bottomLeft += Vector2f(0.01f, 0.0f);
	topLeft += Vector2f(0.01f, 0.0f);
	bottomRight -= Vector2f(0.01f, 0.0f);
}
