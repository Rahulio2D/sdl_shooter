#include "UI/UIElement.h"
#include "Game.h"
#include <SDL.h>

UIElement::UIElement(Vector2i offset, ANCHOR_POINT anchorPoint, ANCHOR_POINT pivotPoint)
{
	this->offset = offset;
	this->anchor = anchorPoint;
	this->pivot = pivotPoint;
	this->offset = offset;
	this->anchor = anchorPoint;
	this->pivot = pivotPoint;
	this->position = Vector2i(0, 0);
	this->size = Vector2i(0, 0);
	this->texture = nullptr;
	this->destRect = nullptr;
	this->sourceRect = nullptr;
}

UIElement::~UIElement()
{
	delete destRect;
	delete sourceRect;
	texture = nullptr;
}

void UIElement::Render()
{
	SDL_RenderCopy(Game::GET_RENDERER(), texture, sourceRect, destRect);
}

void UIElement::SetOffset(Vector2i offset)
{
	this->offset = offset;
	SetUIPosition();
}

void UIElement::SetUIPosition()
{
	Vector2i windowSize = Vector2i(Game::WIDTH, Game::HEIGHT);
	switch(anchor)
	{
		case ANCHOR_POINT::TOP_LEFT:
			anchorPoint = Vector2i(0, 0);
			break;
		case ANCHOR_POINT::TOP_CENTER:
			anchorPoint = Vector2i(windowSize.x / 2, 0);
			break;
		case ANCHOR_POINT::TOP_RIGHT:
			anchorPoint = Vector2i(windowSize.x, 0);
			break;
		case ANCHOR_POINT::LEFT:
			anchorPoint = Vector2i(0, windowSize.y / 2);
			break;
		case ANCHOR_POINT::CENTER:
			anchorPoint = Vector2i(windowSize.x / 2, windowSize.y / 2);
			break;
		case ANCHOR_POINT::RIGHT:
			anchorPoint = Vector2i(windowSize.x, windowSize.y / 2);
			break;
		case ANCHOR_POINT::BOTTOM_LEFT:
			anchorPoint = Vector2i(0, windowSize.y);
			break;
		case ANCHOR_POINT::BOTTOM_CENTER:
			anchorPoint = Vector2i(windowSize.x / 2, windowSize.y);
			break;
		case ANCHOR_POINT::BOTTOM_RIGHT:
			anchorPoint = Vector2i(windowSize.x, windowSize.y);
			break;
	}

	switch(pivot)
	{
		case ANCHOR_POINT::TOP_LEFT:
			pivotPoint = Vector2i(0, 0);
			break;
		case ANCHOR_POINT::TOP_CENTER:
			pivotPoint = Vector2i(size.x / 2, 0);
			break;
		case ANCHOR_POINT::TOP_RIGHT:
			pivotPoint = Vector2i(size.x, 0);
			break;
		case ANCHOR_POINT::LEFT:
			pivotPoint = Vector2i(0, size.y / 2);
			break;
		case ANCHOR_POINT::CENTER:
			pivotPoint = Vector2i(size.x / 2, size.y / 2);
			break;
		case ANCHOR_POINT::RIGHT:
			pivotPoint = Vector2i(size.x, size.y / 2);
			break;
		case ANCHOR_POINT::BOTTOM_LEFT:
			pivotPoint = Vector2i(0, size.y);
			break;
		case ANCHOR_POINT::BOTTOM_CENTER:
			pivotPoint = Vector2i(size.x / 2, size.y);
			break;
		case ANCHOR_POINT::BOTTOM_RIGHT:
			pivotPoint = Vector2i(size.x, size.y);
			break;
	}

	position = (anchorPoint - pivotPoint) + Vector2i(offset.x, -offset.y);
	destRect = new SDL_Rect{position.x, position.y, size.x, size.y};
}

void UIElement::ResetDestRect()
{
	delete destRect;
	destRect = nullptr;
}

void UIElement::ResetSourceRect()
{
	delete sourceRect;
	sourceRect = nullptr;
}
