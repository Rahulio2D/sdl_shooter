#include "UI\UIText.h"
#include "AssetManager.h"
#include <Game.h>

UIText::UIText(std::string fontPath, uint16_t fontSize, std::string text, Vector2i offset, ANCHOR_POINT anchorPoint, ANCHOR_POINT pivotPoint)
	: UIElement(offset, anchorPoint, pivotPoint)
{
	font = AssetManager::GetFont(fontPath, fontSize);
	this->text = text;
	this->offset = offset;
	this->anchor = anchorPoint;
	this->pivot = pivotPoint;
	this->position = Vector2i(0, 0);
	this->size = Vector2i(0, 0);
	this->texture = nullptr;

	SetText(text);
}

UIText::~UIText()
{
	font = nullptr;
}

void UIText::SetText(std::string text)
{
	this->text = text;
	if(texture != nullptr) SDL_DestroyTexture(texture);
	if(destRect != nullptr) ResetDestRect();

	SDL_Surface* surface = TTF_RenderText_Solid(font->GetFont(), text.c_str(), SDL_Color{255, 255, 255, 255});
	texture = SDL_CreateTextureFromSurface(Game::GET_RENDERER(), surface);
	SDL_FreeSurface(surface);

	SDL_QueryTexture(texture, NULL, NULL, &size.x, &size.y);
	SetUIPosition();
}
