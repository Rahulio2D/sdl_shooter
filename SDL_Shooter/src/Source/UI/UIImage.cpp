#include "UI/UIImage.h"
#include "AssetManager.h"

UIImage::UIImage(std::string texturePath, Vector2i size, Vector2i offset, ANCHOR_POINT anchorPoint, ANCHOR_POINT pivotPoint)
	: UIElement(offset, anchorPoint, pivotPoint)
{
	texture = AssetManager::GetTexture(texturePath)->GetTexture();
	this->offset = offset;
	this->anchor = anchorPoint;
	this->pivot = pivotPoint;
	this->position = Vector2i(0, 0);
	this->size = size;
	SetUIPosition();
}

UIImage::UIImage(std::string texturePath, Vector2i size, SDL_Rect sourceRect, Vector2i offset, ANCHOR_POINT anchorPoint, ANCHOR_POINT pivotPoint)
	: UIElement(offset, anchorPoint, pivotPoint)
{
	texture = AssetManager::GetTexture(texturePath)->GetTexture();
	this->offset = offset;
	this->anchor = anchorPoint;
	this->pivot = pivotPoint;
	this->position = Vector2i(0, 0);
	this->size = size;
	this->sourceRect = new SDL_Rect(sourceRect);
	SetUIPosition();
}

UIImage::~UIImage()
{
	texture = nullptr;
}

void UIImage::SetSourceRect(SDL_Rect sourceRect)
{
	ResetSourceRect();
	this->sourceRect = new SDL_Rect(sourceRect);
}

void UIImage::SetImage(std::string texturePath, SDL_Rect sourceRect)
{
	texture = nullptr;
	if(this->destRect != nullptr) ResetDestRect();
	if(this->sourceRect != nullptr) ResetSourceRect();

	this->texture = AssetManager::GetTexture(texturePath)->GetTexture();
	this->sourceRect = new SDL_Rect(sourceRect);
	SetUIPosition();
}
