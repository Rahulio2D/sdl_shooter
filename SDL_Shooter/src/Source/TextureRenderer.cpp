#include "TextureRenderer.h"
#include "Camera.h"
#include "Game.h"

TextureRenderer::TextureRenderer(Texture* texture, Transform* transform, SDL_Rect sourceRect, unsigned int layerOrder)
{
  this->texture = texture;
  this->transform = transform;
  this->sourceRect = sourceRect;
  destinationRect = { 0, 0, 0, 0 };
  this->layerOrder = layerOrder;
}

TextureRenderer::~TextureRenderer()
{
  texture = nullptr;
  transform = nullptr;
}

void TextureRenderer::SetSourceRect(SDL_Rect sourceRect)
{
  this->sourceRect = sourceRect;
}

SDL_Rect* TextureRenderer::GetDestinationRect()
{
  CalculateDestinationRect();
  return &destinationRect;
}

void TextureRenderer::CalculateDestinationRect()
{
  destinationRect = Camera::GetDestinationRect(transform->position, transform->size);
}

SDL_RendererFlip TextureRenderer::TextureFlipState()
{
  if (!flipX && !flipY) return SDL_FLIP_NONE;
  if (flipX && flipY) return (SDL_RendererFlip)(SDL_FLIP_HORIZONTAL | SDL_FLIP_VERTICAL);

  return flipX ? SDL_FLIP_HORIZONTAL : SDL_FLIP_VERTICAL;
}

void TextureRenderer::Render()
{
  CalculateDestinationRect();
  SDL_RenderCopyEx(
    Game::GET_RENDERER(),
    texture->GetTexture(),
    &sourceRect,
    &destinationRect,
    transform->rotation,
    NULL,
    TextureFlipState()
  );
}