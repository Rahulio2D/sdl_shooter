#include "Camera.h"
#include "Game.h"

float Camera::orthographicSize = 0;
float Camera::unitSize = 0;
Vector2i Camera::center;
Transform* Camera::transform;

Camera::Camera(float orthographicSize = 5)
{
  center = Vector2i(Game::WIDTH / 2, Game::HEIGHT / 2);
  SetOrthographicSize(orthographicSize);
  transform = new Transform();
}

void Camera::SetOrthographicSize(float newOrthographicSize)
{
  orthographicSize = newOrthographicSize;
  unitSize = Game::HEIGHT / (orthographicSize * 2);
}

SDL_Rect Camera::GetDestinationRect(Vector2f position, Vector2f size)
{
  position *= unitSize;
  size *= unitSize;
  return
  {
    (int)(center.x - (unitSize * transform->position.x)) - (int)(size.x / 2) + (int)position.x,
    (int)(center.y + (unitSize * transform->position.y)) - (int)(size.y / 2) - (int)position.y,
    (int)(size.x),
    (int)(size.y)
  };
}

Vector2f Camera::ScreenToWorldUnits(Vector2i screenPosition)
{
  return transform->position + (Vector2f(screenPosition.x - center.x, center.y - screenPosition.y) / unitSize);
}

Vector2i Camera::WorldToScreenCoordinates(Vector2f worldPosition)
{
  return Vector2i(center.x + (unitSize * transform->position.x), center.y - (unitSize * transform->position.y))
    + Vector2i((worldPosition.x - transform->position.x) * unitSize, (transform->position.y - worldPosition.y) * unitSize);
}
