#include "Animator.h"
#include <iostream>

Animator::Animator(TextureRenderer* textureRenderer)
{
	animations = std::map<std::string, Animation*>();
	this->textureRenderer = textureRenderer;
}

Animator::~Animator()
{
	std::map<std::string, Animation*>::iterator iter = animations.begin();
	while (iter != animations.end()) {
		delete iter->second;
		++iter;
	}
	animations.clear();
	currentAnimation = nullptr;
}

void Animator::AddAnimation(std::string animationName, Animation* animation)
{
	Animation* existingAnimation = GetAnimationIfExists(animationName);
	if (existingAnimation != nullptr)
	{
		std::cout << "ERROR: Animation with name " << animationName << " already exists!\n";
		return;
	}

	animations.insert(std::pair<std::string, Animation*>(animationName, animation));
}

void Animator::SetAnimation(std::string animationName)
{
	if (animationName == currentAnimationName) return;

	Animation* animation = GetAnimationIfExists(animationName);
	if (animation == nullptr)
	{
		std::cout << "ERROR: Unable to find animation " << animationName << "!\n";
		return;
	}

	currentAnimationName = animationName;
	currentAnimation = animation;
	currentAnimation->Reset();
}

void Animator::Play()
{
	if (currentAnimation == nullptr) return;

	currentAnimation->Update(textureRenderer);
}

Animation* Animator::GetAnimationIfExists(std::string animationName)
{
	std::map<std::string, Animation*>::iterator iter = animations.find(animationName);
	return iter == animations.end() ? nullptr : iter->second;
}
