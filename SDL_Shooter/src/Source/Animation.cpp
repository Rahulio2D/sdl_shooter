#include "Animation.h"
#include "TimeKeeper.h"

Animation::Animation(std::vector<AnimationFrame*> animationFrames, float timePerFrame, bool loopAnimation)
{
	this->animationFrames = animationFrames;
	this->timePerFrame = timePerFrame;
	this->loopAnimation = loopAnimation;
	currentFrame = 0;
	timeElapsed = 0;
}

Animation::~Animation()
{
	for (auto frame : animationFrames) delete frame;
	animationFrames.clear();
}

void Animation::Update(TextureRenderer* textureRenderer)
{
	if (animationFrames.size() == 0) return;

	timeElapsed += TimeKeeper::GetDeltaTime();
	while (timeElapsed >= timePerFrame)
	{
		if (loopAnimation) currentFrame = (currentFrame + 1) % (int)animationFrames.size();
		else currentFrame += (currentFrame < animationFrames.size() - 1);
		timeElapsed -= timePerFrame;
	}
	textureRenderer->texture = animationFrames[currentFrame]->texture;
	textureRenderer->SetSourceRect(animationFrames[currentFrame]->sourceRect);
}

void Animation::Reset()
{
	currentFrame = 0;
	timeElapsed = 0;
}
