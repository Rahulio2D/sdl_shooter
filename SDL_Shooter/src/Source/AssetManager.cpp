#include "AssetManager.h"

std::map<std::string, Texture*> AssetManager::textures = std::map<std::string, Texture*>();
std::map<std::string, Font*> AssetManager::fonts = std::map<std::string, Font*>();

AssetManager::AssetManager()
{
  textures = std::map<std::string, Texture*>();
}

AssetManager::~AssetManager()
{
  {
    std::map<std::string, Texture*>::iterator iter = textures.begin();
    while(iter != textures.end())
    {
      delete iter->second;
      iter = textures.erase(iter);
    }
  }
  
  {
    std::map<std::string, Font*>::iterator iter = fonts.begin();
    while(iter != fonts.end())
    {
      delete iter->second;
      iter = fonts.erase(iter);
    }
  }
}

Texture* AssetManager::GetTexture(std::string texturePath)
{
  auto iter = textures.find(texturePath);
  if(iter != textures.end()) return iter->second; 
  
  return CreateTexture(texturePath);
}

Font* AssetManager::GetFont(std::string fontPath, uint16_t fontSize)
{
  std::string fontName = fontPath + std::to_string(fontSize);
  auto iter = fonts.find(fontName);
  if(iter != fonts.end()) return iter->second;

  return CreateFont(fontPath, fontSize);
}

Texture* AssetManager::CreateTexture(std::string texturePath)
{
  Texture* texture = new Texture(texturePath.c_str());
  textures.insert(std::pair<std::string, Texture*>(texturePath, texture));
  return texture;
}

Font* AssetManager::CreateFont(std::string fontPath, uint16_t fontSize)
{
  Font* font = new Font(fontPath.c_str(), fontSize);
 
  std::string fontName = fontPath + std::to_string(fontSize);
  fonts.insert(std::pair<std::string, Font*>(fontName, font));
  return font;
}
