#include "TimeKeeper.h"

float TimeKeeper::deltaTime = 0;
Uint64 TimeKeeper::ticksPreviousFrame = 0;
Uint64 TimeKeeper::ticksThisFrame = 0;

void TimeKeeper::CalculateDeltaTime()
{
  ticksPreviousFrame = ticksThisFrame;
  ticksThisFrame = SDL_GetPerformanceCounter();

  if (ticksPreviousFrame == 0) deltaTime = 0.0f;
  else deltaTime = (float)(ticksThisFrame - ticksPreviousFrame) / SDL_GetPerformanceFrequency();
}