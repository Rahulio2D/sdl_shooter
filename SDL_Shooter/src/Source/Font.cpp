#include "Font.h"

Font::Font(const char* fontPath, uint16_t fontSize)
{
	this->font = TTF_OpenFont(fontPath, fontSize);
}

Font::~Font()
{
	TTF_CloseFont(font);
}
