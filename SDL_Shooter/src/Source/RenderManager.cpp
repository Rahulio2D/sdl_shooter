#include "RenderManager.h"
#include "Camera.h"
#include "Game.h"

std::vector<TextureRenderer*> RenderManager::listOfTextures = std::vector<TextureRenderer*>();

RenderManager::RenderManager()
{
	listOfTextures = std::vector<TextureRenderer*>();
}

RenderManager::~RenderManager()
{
	listOfTextures.clear();
}

// This adds to queue in layer order
void RenderManager::AddToQueue(TextureRenderer* textureRenderer)
{
	for (int i = 0; i < listOfTextures.size(); i++)
	{
		if (listOfTextures[i]->layerOrder >= textureRenderer->layerOrder)
		{
			listOfTextures.insert(listOfTextures.begin() + i, textureRenderer);
			return;
		}
	}
	listOfTextures.push_back(textureRenderer);
}

void RenderManager::ClearQueue()
{
	listOfTextures.clear();
}

void RenderManager::Render()
{
	for (int i = 0; i < listOfTextures.size(); i++)
	{
		TextureRenderer* textureRenderer = listOfTextures[i];
		textureRenderer->Render();
	}
	ClearQueue();
}