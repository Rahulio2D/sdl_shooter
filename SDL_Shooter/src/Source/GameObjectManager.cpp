#include "GameObjectManager.h"
#include "GameObjects/Player.h"
#include "GameObjects/WallTile.h"
#include "GameObjects/Enemy.h"
#include "GameObjects/Crosshair.h"
#include "TimeKeeper.h"

std::vector<GameObject*> GameObjectManager::gameObjects;
std::map<GameObject*, float> GameObjectManager::gameObjectsToDelete;
CollisionManager* GameObjectManager::collisionManager;

GameObjectManager::GameObjectManager()
{
	renderManager = new RenderManager();
	collisionManager = new CollisionManager(Vector2i(20, 13));
	gameObjects = std::vector<GameObject*>();
	gameObjectsToDelete = std::map<GameObject*, float>();
}

GameObjectManager::~GameObjectManager()
{
	ClearLevel();
	delete renderManager;
	delete collisionManager;
}

void GameObjectManager::Initialise()
{
	for(int i = 0; i < gameObjects.size(); i++)
	{
		InitialiseGameObject(gameObjects[i]);
	}
}

void GameObjectManager::Update()
{
	for(int i = 0; i < gameObjects.size(); i++) gameObjects[i]->Update();
	CheckGameObjectsToDelete();
	collisionManager->UpdateCollisionMatrix();
}

void GameObjectManager::Render()
{
	for(int i = 0; i < gameObjects.size(); i++) gameObjects[i]->Render();
	renderManager->Render();
}

void GameObjectManager::AddGameObject(GameObject* gameObject)
{
	gameObjects.push_back(gameObject);
	InitialiseGameObject(gameObject);
}

GameObject* GameObjectManager::FindGameObjectWithName(std::string name)
{
	for(int i = 0; i < gameObjects.size(); i++)
	{
		if(gameObjects[i]->name == name) return gameObjects[i];
	}
	return nullptr;
}

void GameObjectManager::DeleteGameObject(GameObject* gameObject)
{
	for(int i = 0; i < gameObjects.size(); i++)
	{
		if(gameObjects[i] == gameObject)
		{
			gameObjects.erase(gameObjects.begin() + i);
			collisionManager->RemoveCollider(gameObject->boxCollider);
			delete gameObject;
			return;
		}
	}
}

void GameObjectManager::DeleteGameObject(GameObject* gameObject, float timeToDelete)
{
	gameObjectsToDelete.insert(std::pair<GameObject*, float>(gameObject, timeToDelete));
}

void GameObjectManager::ClearLevel()
{
	for(int i = 0; i < gameObjects.size(); i++)
	{
		collisionManager->RemoveCollider(gameObjects[i]->boxCollider);
		delete gameObjects[i];
		gameObjects[i] = nullptr;
	}
	gameObjectsToDelete.clear();
	gameObjects.clear();
}

void GameObjectManager::InitialiseGameObject(GameObject* gameObject)
{
	gameObject->Initialise();
	collisionManager->AddCollider(gameObject->boxCollider);
}

void GameObjectManager::CheckGameObjectsToDelete()
{
	std::map<GameObject*, float>::iterator iter = gameObjectsToDelete.begin();
	while(iter != gameObjectsToDelete.end())
	{
		float& timeToDelete = iter->second;
		timeToDelete -= TimeKeeper::GetDeltaTime();
		if(timeToDelete <= 0)
		{
			DeleteGameObject(iter->first);
			gameObjectsToDelete.erase(iter++);
			continue;
		}
		iter++;
	}
}
