#include "LevelEditor.h"
#include "LevelManager.h"
#include "Camera.h"
#include "Input.h"
#include "Game.h"
#include "GameObjects/Player.h"
#include "GameObjects/FloorTile.h"
#include "GameObjects/WallTile.h"
#include "GameObjects/Enemy.h"
#include <TimeKeeper.h>

LevelEditor::LevelEditor()
{
	for(int i = 0; i < GAMEOBJECT_LIST_LAYERS; i++) gameObjectList[i] = std::vector<GameObjectData>();
	enemiesList = std::vector<GameObjectData>();
	choosableGameObjects = std::vector<GameObjectData>();
	InitialiseChoosableGameObjects();
	currentChosenObject = 0;

	std::vector<BasicObjectData> basicObjectList = LevelManager::LoadLevelData("Assets/BasicLevel.xml");
	for(int i = 0; i < basicObjectList.size(); i++)
	{
		Vector2i position(basicObjectList[i].position.x, basicObjectList[i].position.y);
		GameObjectData data = AddGameObject(position, basicObjectList[i].type);
		SaveGameObjectData(data);
	}
	InitialisePlayerObject();
}

LevelEditor::~LevelEditor()
{
	GameObjectManager::ClearLevel();
	choosableGameObjects.clear();
	for(auto& list : gameObjectList) list.clear();
	enemiesList.clear();
}

void LevelEditor::Update()
{
	SwitchObjectOnKeyPress();
	if(Input::GetMouseButton(0)) AddGameObjectOnClick();
	if(Input::GetMouseButton(2)) RemoveGameObjectOnClick();
	
	ZoomCamera();
	PanCamera();

	if(Input::GetKeyDown(SDLK_o)) Camera::transform->position = Vector2f(0, 0);		// Reset camera to center
	if(Input::GetKey(SDLK_LCTRL) && Input::GetKeyDown(SDLK_s)) SaveLevelToXML();	// Save level on CTRL + s
}

void LevelEditor::Render()
{
	RenderGameObjects();
	RenderLevelGrid();
}

void LevelEditor::RenderLevelGrid()
{
	float aspectRatio = (float)Game::WIDTH / Game::HEIGHT;
	Vector2f windowUnits = Vector2f(aspectRatio, 1.0f) * Camera::GetOrthographicSize() * 2.0f;	// Window size in game units
	Vector2f windowSize = Vector2f(Game::WIDTH, Game::HEIGHT);																	// Window size in pixels

	float unitSize = Camera::GetUnitSize();
	Vector2f cameraPosition = Camera::transform->position;
	Vector2f offset = (Vector2f(round(cameraPosition.x), round(cameraPosition.y)) - cameraPosition);	// Camera offset (units) 
	Vector2i bottomLeft = Camera::WorldToScreenCoordinates(windowUnits / -2.0f);	// Bottom left of window (screen coordinates)
	Vector2i topRight = Camera::WorldToScreenCoordinates(windowUnits / 2.0f);			// Top right of window (screen coordinates)

	SDL_SetRenderDrawColor(Game::GET_RENDERER(), 200, 200, 200, 200);

	for(int i = 0; i <= (int)windowUnits.x; i++)
	{
		int xPosition = (offset.x * unitSize) + (windowSize.x / 2.0f) - (((int)windowUnits.x / 2) * unitSize) + (unitSize * i) - (unitSize / 2.0f);
		SDL_RenderDrawLine(Game::GET_RENDERER(), xPosition, bottomLeft.y, xPosition, topRight.y);
	}

	for(int i = 0; i < (int)windowUnits.y + 1; i++)
	{
		int yPosition = (offset.y * unitSize * -1) + (windowSize.y / 2.0f) - (((int)windowUnits.y / 2) * unitSize) + (unitSize * i) - (unitSize / 2.0f);
		SDL_RenderDrawLine(Game::GET_RENDERER(), bottomLeft.x, yPosition, topRight.x, yPosition);
	}
}

void LevelEditor::RenderGameObjects()
{
	RenderManager::Render();
}

void LevelEditor::SaveLevelToXML()
{
	tinyxml2::XMLDocument document;
	tinyxml2::XMLNode* root = document.NewElement("level");
	document.InsertFirstChild(root);
	for(int i = 0; i < GAMEOBJECT_LIST_LAYERS; i++)
		for(int j = 0; j < gameObjectList[i].size(); j++)
			AddToXMLDocument(document, root, gameObjectList[i][j]);

	for(int i = 0; i < enemiesList.size(); i++) AddToXMLDocument(document, root, enemiesList[i]);

	AddToXMLDocument(document, root, playerObject);

	tinyxml2::XMLError result = document.SaveFile("Assets/BasicLevel.xml");
}

void LevelEditor::AddToXMLDocument(tinyxml2::XMLDocument& document, tinyxml2::XMLNode* rootNode, GameObjectData& data)
{
	tinyxml2::XMLElement* element = document.NewElement("gameObject");
	tinyxml2::XMLElement* typeElement = element->InsertNewChildElement("type");
	typeElement->SetText(data.type.c_str());

	std::string positionString = std::to_string(data.position.x) + ',' + std::to_string(data.position.y);
	tinyxml2::XMLElement* positionElement = element->InsertNewChildElement("position");
	positionElement->SetText(positionString.c_str());

	element->InsertEndChild(typeElement);
	element->InsertEndChild(positionElement);
	rootNode->InsertEndChild(element);
}

void LevelEditor::InitialiseChoosableGameObjects()
{
	choosableGameObjects.push_back({"Wall", Vector2i(0, 0)});
	choosableGameObjects.push_back({"Floor", Vector2i(0, 0)});
	choosableGameObjects.push_back({"Player", Vector2i(0, 0)});
	choosableGameObjects.push_back({"Enemy", Vector2i(0, 0)});
	currentChosenObject = 0;
}

void LevelEditor::SwitchObjectOnKeyPress()
{
	if(Input::GetKeyDown(SDLK_0)) currentChosenObject = 0;
	else if(Input::GetKeyDown(SDLK_1)) currentChosenObject = 1;
	else if(Input::GetKeyDown(SDLK_2)) currentChosenObject = 2;
	else if(Input::GetKeyDown(SDLK_3)) currentChosenObject = 3;
}

// Add a game object by left clicking
void LevelEditor::AddGameObjectOnClick()
{
	Vector2f mousePos = Camera::ScreenToWorldUnits(Input::GetMousePosition());
	Vector2i objectPosition = Vector2i(round(mousePos.x), round(mousePos.y));

	// If gameobject at current position already, remove it (basically overwriting)
	RemoveGameObjectAtPosition(objectPosition, choosableGameObjects[currentChosenObject].type);

	GameObjectData data;
	if(CurrentGameObjectType().compare("Wall") == 0) data = AddGameObject(objectPosition, GetWallType(objectPosition));
	else data = AddGameObject(objectPosition, CurrentGameObjectType());
	SaveGameObjectData(data);
	UpdateNeighbourWalls(objectPosition);
}

// Remove a gameobject by right clicking
void LevelEditor::RemoveGameObjectOnClick()
{
	Vector2f mousePos = Camera::ScreenToWorldUnits(Input::GetMousePosition());
	Vector2i objectPosition = Vector2i(round(mousePos.x), round(mousePos.y));

	RemoveGameObject(objectPosition);
	UpdateNeighbourWalls(objectPosition);
}

void LevelEditor::RemoveGameObjectAtPosition(Vector2i position, std::string type)
{
	if(type.compare("Player") == 0) return;
	else if(type.compare("Enemy") == 0)
	{
		for(int i = 0; i < enemiesList.size(); i++)
		{
			if(enemiesList[i].position == position)
			{
				GameObjectManager::DeleteGameObject(enemiesList[i].gameObject);
				enemiesList.erase(enemiesList.begin() + i);
				return;
			}
		}
	}
	else
	{
		std::vector<GameObjectData>& list = gameObjectList[GetHashIndex(position)];
		for(int i = 0; i < list.size(); i++)
		{
			if(list[i].position == position)
			{
				GameObjectManager::DeleteGameObject(list[i].gameObject);
				list.erase(list.begin() + i);
				break;
			}
		}
	}
}

// Zoom camera in/out by using the scroll wheel
void LevelEditor::ZoomCamera()
{
	float orthoSize = Camera::GetOrthographicSize();
	float scrollY = Input::GetMouseScrollY();
	if(scrollY != 0)
	{
		orthoSize = MathHelper::Clamp(orthoSize - scrollY, 2.0f, 20.0f);
		Camera::SetOrthographicSize(orthoSize);
	}
}

// Pan camera by holding middle mouse button and moving around
void LevelEditor::PanCamera()
{
	if(Input::GetMouseButtonDown(1))
		mouseHeldDownPosition = Camera::ScreenToWorldUnits(Input::GetMousePosition());

	if(Input::GetMouseButton(1))
	{
		Vector2f difference = mouseHeldDownPosition - Camera::ScreenToWorldUnits(Input::GetMousePosition());
		Camera::transform->position += difference;
	}
}

GameObjectData* LevelEditor::GetGameObjectData(Vector2i position)
{
	auto& list = gameObjectList[GetHashIndex(position)];
	for(int i = 0; i < list.size(); i++)
	{
		if(list[i].position == position) return &list[i];
	}

	return nullptr;
}

std::string LevelEditor::GetWallType(Vector2i position)
{
	bool tileAbove, tileBelow, tileLeft, tileRight;
	tileAbove = tileBelow = tileLeft = tileRight = false;

	if(GetGameObjectData(Vector2i(position.x - 1, position.y)) != nullptr) tileLeft = true;
	if(GetGameObjectData(Vector2i(position.x + 1, position.y)) != nullptr) tileRight = true;
	if(GetGameObjectData(Vector2i(position.x, position.y + 1)) != nullptr) tileAbove = true;
	if(GetGameObjectData(Vector2i(position.x, position.y - 1)) != nullptr) tileBelow = true;

	if(tileBelow && !tileAbove)
	{
		if(tileLeft && !tileRight) return "TopRightWall";
		else if(tileRight && !tileLeft) return "TopLeftWall";
		return "TopWall";
	}
	else if(tileAbove && !tileBelow)
	{
		if(tileLeft && !tileRight) return "BottomRightWall";
		else if(tileRight && !tileLeft) return "BottomLeftWall";
		return "BottomWall";
	}
	else
	{
		if(tileAbove && tileBelow)
		{
			if(tileLeft && !tileRight) return "RightWall";
			else if(tileRight && !tileLeft) return "LeftWall";
			else if (tileLeft && tileRight)
			{
				std::string leftTileType = GetGameObjectData(Vector2i(position.x - 1, position.y))->type;
				std::string rightTileType = GetGameObjectData(Vector2i(position.x + 1, position.y))->type;
				std::string aboveTileType = GetGameObjectData(Vector2i(position.x, position.y + 1))->type;
				std::string belowTileType = GetGameObjectData(Vector2i(position.x, position.y - 1))->type;

				if(aboveTileType.find("Wall") != std::string::npos && belowTileType.find("Floor") != std::string::npos) return "Wall";

				if(leftTileType.find("Wall") != std::string::npos && rightTileType.find("Floor") != std::string::npos)
					return "LeftCornerWall";
				else if(rightTileType.find("Wall") != std::string::npos && leftTileType.find("Floor") != std::string::npos)
					return "RightCornerWall";
			}
		}
		else
		{
			if(tileLeft && !tileRight) return "RightWall";
			else if(tileRight && !tileLeft) return "LeftWall";
		}
	}

	return "Wall";
}

void LevelEditor::UpdateNeighbourWalls(Vector2i position)
{
	for(int i = 0; i < 3; i++)
	{
		for(int j = 0; j < 3; j++)
		{
			if(i == 1 && j == 1) continue;

			Vector2i objectPosition = Vector2i(position.x + (i - 1), position.y + (j - 1));
			GameObjectData* object = GetGameObjectData(objectPosition);
			if(object != nullptr)
			{
				if(object->type.find("Wall") == std::string::npos) continue;	// This is not a wall

				RemoveGameObject(objectPosition);
				GameObjectData data = AddGameObject(objectPosition, GetWallType(objectPosition));
				SaveGameObjectData(data);
			}
		}
	}
}

std::string LevelEditor::CurrentGameObjectType()
{
	return choosableGameObjects[currentChosenObject].type;
}

void LevelEditor::InitialisePlayerObject()
{
	if(playerObject.gameObject != nullptr) return;	// Player already exists

	playerObject = GameObjectData();
	playerObject.type = "Player";
	playerObject.position = Vector2i(0, 0);
	playerObject.gameObject = GameObjectFromType("Player");
	playerObject.gameObject->transform->position = Vector2f(0, 0);
	GameObjectManager::AddGameObject(playerObject.gameObject);
}

void LevelEditor::RemoveGameObject(Vector2i position)
{
	for(int i = 0; i < enemiesList.size(); i++)
	{
		if(enemiesList[i].position == position)
		{
			GameObjectManager::DeleteGameObject(enemiesList[i].gameObject);
			enemiesList.erase(enemiesList.begin() + i);
		}
	}

	std::vector<GameObjectData>& list = gameObjectList[GetHashIndex(position)];
	for(int i = 0; i < list.size(); i++)
	{
		if(list[i].position == position)
		{
			GameObjectManager::DeleteGameObject(list[i].gameObject);
			list.erase(list.begin() + i);
		}
	}
}

GameObjectData LevelEditor::AddGameObject(Vector2i position, std::string type)
{
	GameObjectData data;
	data.type = type;
	data.position = position;
	data.gameObject = GameObjectFromType(type);
	data.gameObject->transform->position = Vector2f(position.x, position.y);
	return data;
}


void LevelEditor::SaveGameObjectData(GameObjectData data)
{
	if(data.type.compare("Player") == 0)
	{
		if(playerObject.gameObject != nullptr)
			GameObjectManager::DeleteGameObject(playerObject.gameObject);

		playerObject = data;
	}
	else if(data.type.compare("Enemy") == 0) enemiesList.push_back(data);
	else gameObjectList[GetHashIndex(data.position)].push_back(data);

	GameObjectManager::AddGameObject(data.gameObject);
}

GameObject* LevelEditor::GameObjectFromType(std::string type)
{
	if(type == "Wall") return new WallTile();
	else if(type == "LeftWall") return new WallTile(WallTileType::LEFT);
	else if(type == "RightWall") return new WallTile(WallTileType::RIGHT);
	else if(type == "TopWall") return new WallTile(WallTileType::TOP);
	else if(type == "BottomWall") return new WallTile(WallTileType::BOTTOM);
	else if(type == "TopLeftWall") return new WallTile(WallTileType::TOPLEFT);
	else if(type == "TopRightWall") return new WallTile(WallTileType::TOPRIGHT);
	else if(type == "BottomLeftWall") return new WallTile(WallTileType::BOTTOMLEFT);
	else if(type == "BottomRightWall") return new WallTile(WallTileType::BOTTOMRIGHT);
	else if(type == "LeftCornerWall") return new WallTile(WallTileType::LEFTCORNER);
	else if(type == "RightCornerWall") return new WallTile(WallTileType::RIGHTCORNER);
	else if(type == "Floor") return new FloorTile();
	else if(type == "Player") return new Player();
	else if(type == "Enemy") return new Enemy();
	else return nullptr;
}

unsigned int LevelEditor::GetHashIndex(Vector2i position)
{
	int index = (position.x + position.y) % GAMEOBJECT_LIST_LAYERS;
	if(index < 0) index += GAMEOBJECT_LIST_LAYERS;
	return index;
}
