#include "Game.h"
#include "LevelManager.h"
#include "UIManager.h"
#include <iostream>

SDL_Renderer* Game::renderer = nullptr;
unsigned int Game::WIDTH = 0;
unsigned int Game::HEIGHT = 0;
bool Game::isMainMenu = true;
bool Game::isLevelEditor = false;
bool Game::isRunning = false;
std::string Game::currentScene = "Uninitialised";

Game::Game(const char* title, int xPos, int yPos, int width, int height, bool fullscreen)
{
	window = nullptr;
	camera = nullptr;
	input = nullptr;
	textureManager = nullptr;
	gameObjectManager = nullptr;
	
	if (InitialiseSDL(title, xPos, yPos, width, height, fullscreen) == 0)
		isRunning = true;

	WIDTH = width;
	HEIGHT = height;
}

Game::~Game()
{
	delete camera;
	delete input;
	delete textureManager;
	delete gameObjectManager;
	delete levelEditor;
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	SDL_Quit();
}

void Game::Initialise()
{
	camera = new Camera(8);
	input = new Input();
	textureManager = new AssetManager();
	gameObjectManager = new GameObjectManager();
	gameObjectManager->Initialise();

	InitialiseGame();
}

int Game::CreateRenderer()
{
	renderer = SDL_CreateRenderer(window, -1, 0);
	if (!renderer)
	{
		std::cout << "ERROR: Unable to create renderer!\n";
		return -1;
	}
	return 0;
}

void Game::InitialiseGame()
{
	isMainMenu = true;
	isLevelEditor = false;
	currentScene = "MainMenu";
	delete levelEditor;
	levelEditor = nullptr;
	camera->SetOrthographicSize(6);
	camera->transform->position = Vector2f(0, 0);
	LevelManager::LoadLevelObjects("Assets/BasicLevel.xml");
	SDL_ShowCursor(SDL_DISABLE);
}

void Game::InitialiseLevelEditor()
{
	isLevelEditor = true;
	isMainMenu = false;
	currentScene = "LevelEditor";
	levelEditor = new LevelEditor();
	camera->SetOrthographicSize(10);
	camera->transform->position = Vector2f(0, 0);
	SDL_ShowCursor(SDL_ENABLE);
}

void Game::Update()
{
	if(isLevelEditor)
	{
		levelEditor->Update();
		if(Input::GetKeyDown(SDLK_ESCAPE)) InitialiseGame();
	}
	else
	{
		if(isMainMenu)
		{
			if(Input::GetKeyDown(SDLK_ESCAPE)) Quit();
			if(Input::GetKeyDown(SDLK_e))	InitialiseLevelEditor();
			// TODO: Add main menu update logic
		}
		else
		{
			// TODO: Add main game update logic
		}
		gameObjectManager->Update();
	}
}

void Game::HandleEvents()
{
	input->HandleInput();
}

void Game::Render()
{
	SDL_SetRenderDrawColor(renderer, 12, 12, 12, 255);
	SDL_RenderClear(renderer);

	if(isLevelEditor) levelEditor->Render();
	gameObjectManager->Render();

	UIManager::Render();

	SDL_RenderPresent(renderer);
}

int Game::InitialiseSDL(const char* title, int xPos, int yPos, int width, int height, bool fullscreen)
{
	if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
	{
		CreateWindow(title, xPos, yPos, width, height, fullscreen);
		CreateRenderer();
		if (!window || !renderer) return -1;
	}
	if(TTF_Init() == -1) return -1;
	return 0;
}

int Game::CreateWindow(const char* title, int xPos, int yPos, int width, int height, bool fullscreen)
{
	window = SDL_CreateWindow(title, xPos, yPos, width, height, fullscreen);
	if (!window)
	{
		std::cout << "ERROR: Unable to create window!\n";
		return -1;
	}
	return 0;
}