#include "ParticleSystem.h"
#include "RenderManager.h"
#include "TimeKeeper.h"
#include "MathHelper.h"

ParticleSystem::ParticleSystem(Transform* transform, std::string texturePath, unsigned int particleCount, float particleLifetime)
{
	this->transform = transform;
	startPosition = transform->position;

	this->texture = AssetManager::GetTexture(texturePath);
	SDL_Rect sourceRect = SDL_Rect{0, 0, 1, 1};

	for(int i = 0; i < particleCount; i++)
	{
		Particle* particle = new Particle();
		particle->transform = new Transform();
		particle->transform->position = this->transform->position;
		particle->textureRenderer = new TextureRenderer(texture, particle->transform, sourceRect);

		float angle = (360.0f / particleCount) * i;
		particle->velocity = Vector2f(cos(angle), sin(angle));

		particles.push_back(particle);
	}

	this->texture = AssetManager::GetTexture(texturePath);
	this->particleCount = particleCount;
	this->particleLifetime = this->particleTimeRemaining = particleLifetime;

	speed = 5.0f;
	startSize = endSize = Vector2f(1.0f, 1.0f);
	startRotation = endRotation = 0;
	loopEffect = false;
	layerOrder = 0;
}

ParticleSystem::~ParticleSystem()
{
	DeleteParticles();
	texture = nullptr;
}

void ParticleSystem::Update()
{
	particleTimeRemaining -= TimeKeeper::GetDeltaTime();
	if(particleTimeRemaining < 0)
	{
		if(!loopEffect)
		{
			DeleteParticles();
			return;
		}
		for(auto particle : particles) particle->transform->position = startPosition;
		particleTimeRemaining = particleLifetime;
	}

	float n = (1.0f / particleLifetime) * particleTimeRemaining;
	for(int i = 0; i < particleCount; i++)
	{
		particles[i]->transform->position += particles[i]->velocity * TimeKeeper::GetDeltaTime() * speed;
		particles[i]->transform->rotation = MathHelper::Lerp(endRotation, startRotation, n);
		particles[i]->transform->size = Vector2f::Lerp(endSize, startSize, n);
		particles[i]->textureRenderer->layerOrder = layerOrder;
	}
}

void ParticleSystem::Render()
{
	if(particleTimeRemaining < 0) return;

	for(int i = 0; i < particleCount; i++)
	{
		RenderManager::AddToQueue(particles[i]->textureRenderer);
	}
}

void ParticleSystem::DeleteParticles()
{
	for(int i = 0; i < particles.size(); i++)
	{
		delete particles[i]->transform;
		delete particles[i]->textureRenderer;
		delete particles[i];
	}
	particles.clear();
}
