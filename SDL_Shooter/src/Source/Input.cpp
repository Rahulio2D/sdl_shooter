#include "Input.h"
#include "Game.h"

std::vector<SDL_Keycode> Input::keysCurrentlyHeld;
std::vector<SDL_Keycode> Input::keysPressedThisFrame;
std::vector<SDL_Keycode> Input::keysReleasedThisFrame;
bool Input::mouseButtonsCurrentlyHeld[3] = {0, 0, 0};
bool Input::mouseButtonsPressedThisFrame[3] = {0, 0, 0};
bool Input::mouseButtonsReleasedThisFrame[3] = {0, 0, 0};
int Input::mouseX = 0;
int Input::mouseY = 0;
float Input::mouseScrollX = 0;
float Input::mouseScrollY = 0;

Input::Input()
{
  keysCurrentlyHeld = std::vector<SDL_Keycode>();
  keysPressedThisFrame = std::vector<SDL_Keycode>();
  keysReleasedThisFrame = std::vector<SDL_Keycode>();
}

Input::~Input() 
{
  keysCurrentlyHeld.clear();
  keysPressedThisFrame.clear();
  keysReleasedThisFrame.clear();
}

void Input::HandleInput()
{
  ClearInput();

  SDL_Event event;
  SDL_PollEvent(&event);
  switch(event.type)
  {
    case SDL_KEYDOWN:
      AddKeyPressed(event.key.keysym.sym);
      break;
    case SDL_KEYUP:
      AddKeyReleased(event.key.keysym.sym);
      break;
    case SDL_MOUSEBUTTONDOWN:
      AddMouseButtonPressed(event.button.button);
      break;
    case SDL_MOUSEBUTTONUP:
      AddMouseButtonReleased(event.button.button);
      break;
    case SDL_MOUSEWHEEL:
      SetMouseScrollWheel(event.wheel);
      break;
    case SDL_QUIT:
      Game::Quit();
      break;
    default:
      break;
  }
  SetMousePosition();
}

void Input::ClearInput()
{
  keysPressedThisFrame.clear();
  keysReleasedThisFrame.clear();
  for(int i = 0; i < 3; i++)
  {
    mouseButtonsPressedThisFrame[i] = false;
    mouseButtonsReleasedThisFrame[i] = false;
  }
  mouseScrollX = mouseScrollY = 0;
}

void Input::AddKeyPressed(SDL_Keycode key)
{
  if(!FoundKey(&keysCurrentlyHeld, key) && AddUniqueKey(&keysPressedThisFrame, key) != -1)
    keysCurrentlyHeld.push_back(key);
}

void Input::AddKeyReleased(SDL_Keycode key)
{
  if (AddUniqueKey(&keysReleasedThisFrame, key) != -1)
  {
    for (int i = 0; i < keysCurrentlyHeld.size(); i++)
    {
      if (keysCurrentlyHeld[i] == key)
        keysCurrentlyHeld.erase(keysCurrentlyHeld.begin() + i);
    }
  }
}

void Input::AddMouseButtonPressed(int button)
{
  button -= 1;
  if(!ValidMouseButton(button)) return;

  mouseButtonsCurrentlyHeld[button] = true;
  mouseButtonsPressedThisFrame[button] = true;
}

void Input::AddMouseButtonReleased(int button)
{
  button -= 1;
  if(!ValidMouseButton(button)) return;

  mouseButtonsCurrentlyHeld[button] = false;
  mouseButtonsReleasedThisFrame[button] = true;
}

void Input::SetMousePosition()
{
  SDL_GetMouseState(&mouseX, &mouseY);
}

void Input::SetMouseScrollWheel(SDL_MouseWheelEvent event)
{
  mouseScrollX = event.x;
  mouseScrollY = event.y;
}

bool Input::GetKey(SDL_Keycode key)
{
  return FoundKey(&keysCurrentlyHeld, key);
}

bool Input::GetKeyDown(SDL_Keycode key)
{
  return FoundKey(&keysPressedThisFrame, key);
}

bool Input::GetKeyUp(SDL_Keycode key)
{
  return FoundKey(&keysReleasedThisFrame, key);
}

bool Input::GetMouseButton(int button)
{
  if(!ValidMouseButton(button)) return false;

  return mouseButtonsCurrentlyHeld[button];
}

bool Input::GetMouseButtonDown(int button)
{
  if(!ValidMouseButton(button)) return false;

  return mouseButtonsPressedThisFrame[button];
}

bool Input::GetMouseButtonUp(int button)
{
  if(!ValidMouseButton(button)) return false;

  return mouseButtonsReleasedThisFrame[button];
}

int Input::AddUniqueKey(std::vector<SDL_Keycode>* keysVector, SDL_Keycode key)
{
  if (FoundKey(keysVector, key)) return -1;

  keysVector->push_back(key);
  return 0;
}

bool Input::FoundKey(std::vector<SDL_Keycode>* keysVector, SDL_Keycode key)
{
  for (int i = 0; i < keysVector->size(); i++)
    if (keysVector->at(i) == key) return true;
  return false;
}
