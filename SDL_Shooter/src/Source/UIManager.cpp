#include "UIManager.h"

std::unordered_map<std::string, UIText*> UIManager::textElements;
std::unordered_map<std::string, UIImage*> UIManager::imageElements;

UIManager::UIManager()
{
	textElements = std::unordered_map<std::string, UIText*>();
	imageElements = std::unordered_map<std::string, UIImage*>();
}

UIManager::~UIManager()
{
	for(auto const& text : textElements) delete text.second;
	for(auto const& image : imageElements) delete image.second;

	textElements.clear();
	imageElements.clear();
}

UIText* UIManager::CreateText(UIText* text, std::string name)
{
	if(text == nullptr) return nullptr;

	textElements.insert(std::pair<std::string, UIText*>(name, text));
	return text;
}

UIImage* UIManager::CreateImage(UIImage* image, std::string name)
{
	if(image == nullptr) return nullptr;

	imageElements.insert(std::pair<std::string, UIImage*>(name, image));
	return image;
}

UIText* UIManager::GetText(std::string name)
{
	for(auto const& text : textElements)
	{
		if(text.first == name) return text.second;
	}
	return nullptr;
}

UIImage* UIManager::GetImage(std::string name)
{
	for(auto const& image : imageElements)
	{
		if(image.first == name) return image.second;
	}
	return nullptr;
}

bool UIManager::RemoveText(std::string name, UIText* text)
{
	for(auto const& textElement : textElements)
	{
		if(textElement.first == name)
		{
			delete text;
			textElements.erase(name);
			return true;
		}
	}
	return false;
}

bool UIManager::RemoveImage(std::string name, UIImage* image)
{
	for(auto const& imageElement : imageElements)
	{
		if(imageElement.first == name)
		{
			delete image;
			imageElements.erase(name);
			return true;
		}
	}
	return false;
}

void UIManager::Render()
{
	for(auto const& image : imageElements) image.second->Render();
	for(auto const& text : textElements) text.second->Render();
}
