#include "LevelManager.h"
#include "GameObjectManager.h"
#include "GameObjects/Player.h"
#include "GameObjects/Enemy.h"
#include "GameObjects/WallTile.h"
#include "GameObjects/FloorTile.h"
#include <tinyxml2.cpp>
#include <iostream>
#include <sstream>

std::vector<BasicObjectData> LevelManager::gameObjectList = std::vector<BasicObjectData>();

std::vector<BasicObjectData> LevelManager::LoadLevelData(std::string levelPath)
{
	UnloadLevel();
	LoadDataFromXmlFile(levelPath);

	return gameObjectList;
}

void LevelManager::LoadLevelObjects(std::string levelPath)
{
	UnloadLevel();
	LoadDataFromXmlFile(levelPath);

	for(int i = 0; i < gameObjectList.size(); i++) {
		GameObject* gameObject = GameObjectType(gameObjectList[i].type);
		gameObject->transform->position = gameObjectList[i].position;
		GameObjectManager::AddGameObject(gameObject);
		gameObject = nullptr;
	}
}

void LevelManager::UnloadLevel()
{
	GameObjectManager::ClearLevel();
	gameObjectList.clear();
}

void LevelManager::LoadDataFromXmlFile(std::string levelPath)
{
	tinyxml2::XMLDocument doc;
	if(doc.LoadFile(levelPath.c_str()) != 0)
	{
		std::cout << "ERROR: UNABLE TO READ LEVEL FILE: " << levelPath << '\n';
		return;
	}

	tinyxml2::XMLElement* rootNode = doc.FirstChildElement("level");
	if(rootNode == nullptr)
	{
		std::cout << "ERROR WHILE READING ROOT NODE OF LEVEL\n";
		return;
	}

	tinyxml2::XMLElement* gameObjectNode = rootNode->FirstChildElement("gameObject");
	if(gameObjectNode == nullptr)
	{
		std::cout << "ERROR UNABLE TO FIND GAMEOBJECTS IN LEVEL\n";
		return;
	}

	while(gameObjectNode != nullptr)
	{
		BasicObjectData data;
		GameObject* gameObject = GameObjectType(gameObjectNode->FirstChildElement("type")->GetText());
		if(gameObject == nullptr)
		{
			gameObjectNode = gameObjectNode->NextSiblingElement();
			continue;
		}
		data.type = gameObjectNode->FirstChildElement("type")->GetText();

		Vector2f position;
		if(GameObjectPosition(gameObjectNode->FirstChildElement("position")->GetText(), position) != 0)
		{
			gameObjectNode = gameObjectNode->NextSiblingElement();
			continue;
		}
		data.position = position;

		delete gameObject;
		gameObjectList.push_back(data);
		gameObjectNode = gameObjectNode->NextSiblingElement();
	}
}

GameObject* LevelManager::GameObjectType(std::string type)
{
	if(type == "Wall") return new WallTile();
	else if(type == "LeftWall") return new WallTile(WallTileType::LEFT);
	else if(type == "RightWall") return new WallTile(WallTileType::RIGHT);
	else if(type == "TopWall") return new WallTile(WallTileType::TOP);
	else if(type == "BottomWall") return new WallTile(WallTileType::BOTTOM);
	else if(type == "TopLeftWall") return new WallTile(WallTileType::TOPLEFT);
	else if(type == "TopRightWall") return new WallTile(WallTileType::TOPRIGHT);
	else if(type == "BottomLeftWall") return new WallTile(WallTileType::BOTTOMLEFT);
	else if(type == "BottomRightWall") return new WallTile(WallTileType::BOTTOMRIGHT);
	else if(type == "LeftCornerWall") return new WallTile(WallTileType::LEFTCORNER);
	else if(type == "RightCornerWall") return new WallTile(WallTileType::RIGHTCORNER);
	else if(type == "Floor") return new FloorTile();
	else if(type == "Player") return new Player();
	else if(type == "Enemy") return new Enemy();
	else return nullptr;
}

int LevelManager::GameObjectPosition(std::string positionString, Vector2f& position)
{
	for(unsigned int i = 0; i < positionString.length(); i++)
	{
		if(positionString[i] == ',')
		{
			position.x = std::stof(positionString.substr(0, i));
			position.y = std::stof(positionString.substr(i + 1, positionString.length() - i - 1));
			return 0;
		}
	}
	return -1;
}
