#include "BoxCollider.h"

BoxCollider::BoxCollider(Transform* transform, Vector2f offset, Vector2f size)
{
  this->transform = transform;
  this->offset = offset;
  this->size = size;
}

BoxCollider::~BoxCollider()
{
  transform = nullptr;
}